@echo off
if ~%1==%1~ goto SYNTAX

echo [Creating master file] ...
echo.
mx1660.exe iso=%1 convert=ansi create=LILACS -all now tell=1000

echo [Creating inverted file]
echo.
mx1660.exe LILACS fst=@LILACS.fst fullinv/ansi=LILACS -all now tell=1000

echo [Database created and inverted]
echo.

goto END

:SYNTAX
echo.
echo  Use: gencertif [iso file] 
echo.

:END
