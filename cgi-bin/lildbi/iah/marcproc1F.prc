/* ===========================================================	*/
/*  Conversion from LILACS to MARC21				*/
/* ===========================================================	*/
/*  Requires external databases declared in marc.cip		*/
/*  TITLE: full and abridged title				*/
/*  PAISES: countries    2char codes --> 3 chars codes		*/
/*  gizmo=roles, gizmo=idiomas					*/
/*								*/
/*  Requires mx version 4.3 or better				*/
/*  parameters declared  in=lilacsmarc.in			*/
/*	proc=@marcproc.prc					*/
/*	proc=@deletduplic.prc					*/
/*	proc='s'						*/
/*	isotag1=3000						*/
/*	gizmo=g850ans (?)					*/
/*								*/
/*  Leader defined in v3000 + offset, customizable,	  	*/
/*  for compatibility with Isismarc, in agreement with AOT	*/
/*								*/
/*  Holdings declared in fields v3 + v7 are ignored		*/
/*  because this is idiosincratic to each institution		*/
/*								*/
/*  Ernesto Spinak, v.1 mayo/junio 2005				*/
/*  assistance of Adalberto Tardelli & Francisco Lopes Santos	*/
/* (c) BIREME							*/
/* ===========================================================	*/

'd*'

/* prepares previous data for conversion of meetings  */
proc(    
	if p(v52) then '<9000>',v52+|; |,'</9000>', fi   
	)


if p (v2) then '<001>',v2,'</001>',  fi,
if p (v1) then '<003>',v1,'</003>',  fi,
if p(v93) then '<005>',v93,'000000.0</005>', fi,


/*  ==========  FIELD 008 fixed:40 bytes ===========   */
'<008>',
  /* 00-05 */	if a(v91) or size(v91)<>8 then '000000' else v91[1]*2.6  fi,
  /* 06-14 */	if a(v65) or size(v65)<> 8 or s(mhu,v64):'S.D' or s(mhu,v64):'S.F' then mpl, 'n        ' 
		else 
			if v64:'?' or v64:'a' or v64:'[' then mpl, 'q',v65*0.4,'    ',
			else mpl,'e',v65,
			fi,
		fi,
   /* 15-17 */	if a(v67)  or  L(['PAISES']v67[1])=0 then '   ' 
		else 		
			ref(['PAISES']L(['PAISES']v67[1]),v1, if size(v1)<3 then ' ' fi,),
		fi,
   /* 18-20 */	'   ',
   /* 21    */	if v5*0.1='S' then if p(v113) then v113 else ' ' fi, else ' ' fi, 
   /* 22    */     ' ',
   /* 23    */	if p(v114) then v114 else
			if p(v110) then v110 else ' ' fi,
		fi,
   /* 24    */	' ',
   /* 25    */	if p(v112) then v112 else ' ' fi,
   /* 26    */	if p(v111) then v111 else ' ' fi,
   /* 27-28 */	'  ',
   /* 29    */	if p(v115) then v115 else ' ' fi,
   /* 30-32 */	'     ',
   /* 35-37 */	proc('d99','<99>',
			if p(v40) then v40[1],
        	        else
				if p(v12[1]^i) then v12[1]^i,
				else
					if p(v18[1]^i) then v18[1]^i,
					else 
						if p(v25[1]^i) then v25[1]^i fi,
					fi,
				fi,
			fi,				
		'</99>'),
	proc('GIDIOMAS,99'),v99,			
   /* 38-39 */	'  ',
'</008>',
/*  ==========  END FIELD 008      ===========   */


if p(v69) then '<020>  a',v69,'</020>',  fi,
if p(v35) then '<022>  a',v35,'</022>',  fi,
if p(v68) then '<024>8 a',v68,'</024>',  fi,


/* ===========  additional languages: v41 MARC ======*/
/* old LILACS methodology */
if nocc(v40)>1 or p(v41) then 
	'<041>1 ',
                (if iocc>1 then  ref(['IDIOMAS']L(['IDIOMAS']V40),|a|v2)  fi),
                ( ref(['IDIOMAS']L(['IDIOMAS']V41),|b|v2) )
	'</041>',
fi,

/* present LILACS methodology */
if s(v40,v41)='' then
	if p(v12) and nocc(v12)>1 then
                '<041>1 ', (if iocc>1 then ref(['IDIOMAS']L(['IDIOMAS']V12^i),|a|v2) fi), '</041>',
	else
		if p(v18) and nocc(v18)> 1 then
                        '<041>1 ',(if iocc>1 then ref(['IDIOMAS']L(['IDIOMAS']V18^i),|a|v2) fi), '</041>',
		else
			if p(v25) and nocc(v25)>1 then
                                '<041>1 ', (if iocc>1 then ref(['IDIOMAS']L(['IDIOMAS']V25^i),|a|v2) fi),, '</041>',
			fi,
		fi,
	fi,
fi,
if p(v83^i) then '<041>1 ',( ref(['IDIOMAS']L(['IDIOMAS']V83^i),|b|v2) ), '</041>', fi,

/*  =========  end field v41 ================*/



if p(v3^a) then  '<084>1 a',(if p(v3^a) then v3^a,break,fi),'</084>', fi,


/*  ========= 	MAIN ENTRY ======== 	*/
/* roles are converted with a following gizmo in the pipe */
/* 		ANALITICS		*/
if p(v10) then    	/* tested mfn=308026 */
  (if iocc=1 then 
     '<100>1 a',
	v10^*,
        if p(v10^r) then,'e',v10^r fi, 
        if p(v10^1) then 
                'u',v10^1,  |. |v10^2,   |. |v10^3|.|,   | |v10^c|,|,   | |v10^p|.|
	,fi,
     '</100>', 
  else
     '<700>1 a',
	v10^*,
        if p(v10^r) then,'e',v10^r fi,  
        if p(v10^1) then 
                'u',v10^1,  |. |v10^2,   |. |v10^3|.|,   | |v10^c|,|,   | |v10^p|.|
	,fi,
   '</700>', 
  fi/)
fi,

if p(v11) then    	/* tested mfn=368999 */
  (if iocc=1 then 
     '<110>2 a',
	v11^*,
        if p(v11^r) then,'e',v11^r fi,   /* roles are converted with a post gizmo */
     '</110>', 
  else
     '<710>2 a',
	v11^*,
        if p(v11^r) then,'e',v11^r fi,  
   '</710>', 
  fi/)
fi,

/* 	MONOGRAPH	*/
if s(v10,v11,v12)='' then      
	if p(v16) then    	
	  (if iocc=1 then 
                '<100>1 a',
		v16^*,
                if p(v16^r) then,'e',v16^r fi,  
        	if p(v16^1) then 
                        'u',v16^1,  |. |v16^2,   |. |v16^3|.|,   | |v16^c|,|,   | |v16^p|.|
		,fi,
	     '</100>', 
	  else
             '<700>1 a',
		v16^*,
                if p(v16^r) then,'e',v16^r fi,
        	if p(v16^1) then 
                        'u',v16^1,  |. |v16^2,   |. |v16^3|.|,   | |v16^c|,|,   | |v16^p|.|
		,fi,
	   '</700>', 
	  fi/)
	fi,

	if p(v17) then   
	  (if iocc=1 then 
             '<110>2 a',
		v17^*,
                if p(v17^r) then,'e',v17^r fi,  
	     '</110>', 
	  else
             '<710>2 a',
		v17^*,
                if p(v17^r) then,'e',v17^r fi, 
	   '</710>', 
	  fi/)
	fi,
fi,

/* 	COLECTION	*/
if v6='c' then  
	if p(v23) then    	/* NO tested */
	  (if iocc=1 then 
                '<100>1 a',
		v23^*,
                if p(v23^r) then,'e',v23^r fi,  
        	if p(v23^1) then 
                        'u',v23^1,  |. |v23^2,   |. |v23^3|.|,   | |v23^c|,|,   | |v23^p|.|
		,fi,
	     '</100>', 
	  else
             '<700>1 a',
		v23^*,
                if p(v23^r) then,'e',v23^r fi, 
        	if p(v23^1) then 
                        'u',v23^1,  |. |v23^2,   |. |v23^3|.|,   | |v23^c|,|,   | |v23^p|.|
		,fi,
	   '</700>', 
	  fi/)
	fi,

	if p(v24) then   
	  (if iocc=1 then 
             '<110>2 a',
		v24^*,
                if p(v24^r) then,'e',v24^r fi,  
	     '</110>', 
	  else
             '<710>2 a',
		v24^*,
                if p(v24^r) then,'e',v24^r fi,
	   '</710>', 
	  fi/)
	fi,
fi,
/*  ======== END MAIN ENTRY ======== 	*/


/*  ========= TITLE & IMPRINT =======	*/
if v6:'a' then 
        if p(v13) then '<242>10a',v13,  'yeng</242>',  fi,
        if p(v12) then '<245>00a',v12+|=b|, '</245>',  fi,
        if p(v63) then '<250>  a',v63,       '</250>',  fi,
fi,

if v6='m' or v6='ms' then
        if p(v19) then '<242>10a',v19,  'yeng</242>',  fi,
        if p(v18) then '<245>00a',v18+|=b|, '</245>',  fi,
        if p(v63) then '<250>  a',v63,       '</250>',  fi,
fi,

if v6='c' then
        if p(v26) then '<242>10a',v26,  'yeng</242>',  fi,
        if p(v25) then '<245>00a',v25+|=b|, '</245>',  fi,
        if p(v63) then '<250>  a',v63,       '</250>',  fi,
fi,


/* anal�tics has no imprint in 260 */
if v6:'a' then  else             
        '<260>  a',
		if p(v66) then 
			if s(mhu,v66):'S.L' then mpl, '[s.l.]', else mpl,v66 fi,
	        else '[s.l.]' fi,
                ' :b',
		if p(v62) then
			if s(mhu,v62):'S.N' then mpl, '[s.n.]', else mpl,v62 fi,
	        else '[s.n.]' fi,
                ',c',
		if p(v64) then
			if s(mhu,v64):'S.D' then mpl, '[s.d.]', else mpl,v64 fi,
	        else '[s.d.]' fi,
	'</260>',
fi,

if s(v20,v27,v38)<>'' then
	'<300>##',
		if p(v38^a) then v38 
		else 
               		if v6:'as' then 
			else 	
                          'a',if v6='c' then v27" vols." else v20" p." fi, " :"d38
			fi,
                        "b"v38+|, |,
		fi,
 	'</300>',
fi,

/*  =======  SERIES STATEMENT  =======  */
if v6:'a' then else
   if v6:'ms' then
      (if p(v30) then 
                '<440> 0a',v30,  
		if s(v31,v32)<>'' then 
                        ' ;v',  |v. |v31,   
			if p(v31) and p(v32) then ', ' fi,
			|no.|v32,  
		fi,
		'</440>', 
      fi/)
   fi,
fi,


/* ========= Notes ====== */
( if p(v500) then  '<500>  a',v500, '</500>', fi/)

if p(v50) then	
        '<502>  a',v51" -- ",v50,
	", "d66, if s(mhu,v66):'S.L' then mpl, "["v66".]" else mpl,v66 fi,
	", "v65*0.4, 
	'</502>', 
fi,

( if p(v503) then  '<503>  a',v503, '</503>', fi/)
( if p(v72)  then  '<504>  b',v72,  '</504>', fi/)
( if p(v505) then  '<505>  a',v505, '</505>', fi/)
( if p(v83)  then  '<520>  a',v83^*,'</520>', fi/)
( if p(v533) then  '<533>  a',v533, '</533>', fi/)
( if p(v534) then  '<534>  a',v534, '</534>', fi/)

if s(v58,v59,v60)<>'' then
	'<536>  ',
                if p(v58) then 'a',v58+|; |, fi,
                if p(v59) then 'd',v59, fi,
                if p(v60) then 'f',v60, fi,
	'</536>',
fi,

( if p(v61)  then  '<590>  a',v61,  '</590>', fi/)



/* ========== Subject Added Entries ========== */

( if p(v78)  then '<600>14a',v78, '</600>', fi /)
( if p(v610) then '<610>24a',v610,'</610>', fi /)
  if s(v74,v75)<>'' then '<648> 4a',v74,"-"v75, '</648>', fi,
( if p(v76)  then '<650> 2a',v76, '</650>', fi /)

( if p(v87)  then '<650>12a',v87^d,  |x|v87^s, '</650>', fi/)
( if p(v88)  then '<650>22a',v88^d,  |x|v88^s, '</650>', fi/)
( if p(v82)  then '<651> 4a',v82,  '</651>', fi/)
( if p(v653) then '<653>1 a',v653, '</653>', fi/)
( if p(v71)  then '<655> 7a',v71, '2mesh</655>',  fi/)

( if p(v49) then
	if p(v49^1) then
                '<700>1 a',v49^*,  'eths',
                'u',v49^1,  |. |v49^2,   |. |v49^3|.|,   | |v49^c|,|,   | |v49^p|.|,
		'</700>',
	else
                '<720>1 a',v49^*,  'eths', '</720>',
	fi,
fi/)


/* ============= ADDED ENTRIES PERSONAL and CORPORATE NAMES ============ */
/* 	roles are converted with a following gizmo in the pipe 		 */
if v6:'a' then
	( if p(v16) then
                '<700>1 a',
		v16^*,
                if p(v16^r) then,'e',v16^r fi,  
        	if p(v16^1) then 
                        'u',v16^1,  |. |v16^2,   |. |v16^3|.|,   | |v16^c|,|,   | |v16^p|.|
		,fi,
	   	'</700>', 
	fi,/)
	( if p(v17) then
                '<710>2 a',
		v17^*,
                if p(v17^r) then,'e',v17^r fi,  
		'</710>', 
	fi/)
fi,
if v6:'c' then
	if v6='c'then else
		( if p(v23) then
                        '<700>1 a',
			v23^*,
                        if p(v23^r) then,'e',v23^r fi,
        		if p(v23^1) then 
                                'u',v23^1,  |. |v23^2,   |. |v23^3|.|,   | |v23^c|,|,   | |v23^p|.|
			,fi,
		   	'</700>', 
		fi/)
		( if p(v24) then
                        '<710>2 a',
			v24^*,
                        if p(v24^r) then,'e',v24^r fi, 
			'</710>', 
		fi/)
	fi,
fi,

/* ============= ADDED ENTRIES MEETING NAME ============ */

( if p(v53) then 			/* tested mfn=373383  */

        '<711>2 a',
                if v53:', ' then
			left(v53,size(v53)-(11-instr(right(v53,10),','))   ),
                        'n(',  right(v53,(9-instr(right(v53,10),','))),
			if right(v53,(9-instr(right(v53,10),','))) >''  then ' :' fi,
		else  v53| (|  fi,
                |d|v54[1],
                | :c|v56[1],|, |v57[1], ')',
                |e|v9000[1],
	'</711>',
,fi/)

(if p(v52) then '<710>2 a', v52, '</710>', fi/)




/* ============= LINKING FIELDS =========== */

if v6:'a' or v6:'mc' then
        '<773>0 a',

	select s(v6)
		case 'as':,  	if L(['TITLE']v30)>0 then  
                                        ref(['TITLE']L(['TITLE']V30),V100,": "V110,". "V120,", "V130, "d"v140)  
				else v30 fi, 
                                if s(v31,v32)<>'' then 'g', "Vol."v31, ", no."v32  fi,
				" ("v64")", if p(v14^f) then " p."v14^f,"-"v14^l else " p."v14, fi

		case 'am':,  	if nocc(v16)> 3 then v16[1]^*, ' [et al]',
				else v16[1..3]^*+|; |, fi,
				if a(v16) then v17[1]^* fi,	
                                ", t"v18,  ".d"v66, " : "v62, ", "v64, 
                                ".h"v20"p.",
				
		case 'ams':, 	if nocc(v16)> 3 then v16[1]^*, ' [et al]',
				else v16[1..3]^*+|; |, fi,
				if a(v16) then v17[1]^* fi,
                                ", t"v18,  ".d"v66, " : "v62, ", "v64, 
                                ".h"v20"p.",
                                if s(v31,v32)<>'' then 'k', "Vol."V31,  ", no. "v32  fi,
                                "g"v30,

		case 'amc':,  	if nocc(v16)> 3 then v16[1]^*, ' [et al]',
				else v16[1..3]^*+|; |, fi,
				if a(v16) then v17[1]^* fi,	
                                ", t"v18,  ".d"v66, " : "v62, ", "v64, 
                                '.h', "vol."v21, v20"p.",
                                "k"v25, ' / ', v23[1], if a(v23) then v24[1] fi,
				". - "v27" vols.",

		case 'mc':,  	if nocc(v23)> 3 then v23[1]^*, ' [et al]',
				else v23[1..3]^*+|; |, fi,
				if a(v23) then v24[1]^* fi,	
                                ", t"v25,  ". - "v27" vols.",
			
	endsel,
	'</773>',
fi,



/* PROVISIONAL,  ^I WILL BE CHANGED TO ^U  */
(if p(v8) then '<856>4 u',v8^i, |k|v8^k, |l|v8^l, |q|v8^q, |s|v8^s, |x|v8^x,|z|v8^z, '</856>', fi/)


/* ============== LEADER  ================= */
'<3005>n</3005>',
'<3006>',if p(v9) then v9 else 'a' fi, '</3006>',
'<3007>',
	select s(v6)
	case 'm':,	'm', 
	case 'ms':,	'm',
	case 'as':,	'b',
	case 'am':,	'a',
	case 'amc':,	'a',
	case 'ams':,	'a',
	case 'mc':,	'd',
	case 'c':,	'c',
	elsecase,	'b',
	endsel,
'</3007>',

'<3017> </3017>',
'<3018>u</3018>',

/* ===================  END OF SCRIPT   =================== */

/* Sao Paulo on the Tiet�, Annus Domini 2005 */
 

