var optWidth = (HM_WindowWidth / 6)-2; // number of menu options
if (navigator.appName == "Netscape")
	var barraSuperior = 48; 
else
	var barraSuperior = 53;

HM_Array1 = [
[
optWidth, // menu width
10,			// left_position
barraSuperior,			// top_position
"black",     // font_color
"white",    // mouseover_font_color
"#779DC1",  // background_color
"#9999CC",  // mouseover_background_color
"black",    // border_color
"black",  // separator_color
1,			// top_is_permanent
1,          // top_is_horizontal
0,          // tree_is_horizontal
1,          // position_under
0,          // top_more_images_visible
1,          // tree_more_images_visible
"null",     // evaluate_upon_tree_show
"null",     // evaluate_upon_tree_hide
],          //right-to-left
["System",		  "",				                                                    0,0,1],
["Users",	      "javascript:void(load_page('CADASTRO DE USUARIOS.X'))",			    1,0,0],
["Data Base",     "",		                                                            0,0,1],
["Reports",	      "javascript:void(load_page('INFORMACOES GERAIS DA BASE DE DADOS.X'))",1,0,0],
["Change Profile","javascript:void(load_page('OPEN CHANGE PROFILE.X'))",	            1,0,0],
["Exit",          "javascript:void(Close_Window())",     	                            1,0,0]
]

HM_Array1_1 = [
[],
["Status","javascript:void(load_page('ABRE DISPONIBILIDADE.X'))",   1,0,0],
["Notify Users", "javascript:void(load_page('AVISO AOS USUARIOS.X'))",     1,0,0]
]

HM_Array1_3 = [
[],
["Definitions",		        "",	                                                      0,0,1],
["Generate Inverted Files", "javascript:void(load_page('ABRE GERAR INVERTIDO.X'))",   1,0,0],
["Unlock",		            "javascript:void(load_page('MENU DESBLOQUEAR.X'))",	      1,0,0],
["Restart Database",    "javascript:void(load_page('ABRE REINICIA BD.X'))",		  1,0,0]
]

HM_Array1_3_1 = [
[],
["General Definitions", "javascript:void(load_page('DEFINICOES GERAIS.X'))",		   1,0,0],
["Record Types",       "javascript:void(load_page('ABRE DEFINIR TIPOS REGISTROS.X'))",1,0,0],
["Complements",	        "javascript:void(load_page('DEFINIR SUBTIPO DE REGISTRO.X'))", 1,0,0],
["Fields",			    "javascript:void(load_page('DEFINIR CAMPOS.X'))",			   1,0,0],
["FST",				    "javascript:void(load_page('ABRE DEFINE FST.X'))",			   1,0,0],
["Messages",		    "javascript:void(load_page('DEFINIR MENSAGENS.X'))",		   1,0,0]
]
