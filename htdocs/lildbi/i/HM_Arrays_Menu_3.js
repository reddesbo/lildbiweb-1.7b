var optWidth = (HM_WindowWidth / 5)-2; // number of menu options
if (navigator.appName == "Netscape")
	var barraSuperior = 48; 
else
	var barraSuperior = 53;

HM_Array1 = [
[
optWidth, // menu width
10,			// left_position
barraSuperior,			// top_position
"black",     // font_color
"white",    // mouseover_font_color
"#779DC1",  // background_color
"#9999CC",  // mouseover_background_color
"black",    // border_color
"black",  // separator_color
1,			// top_is_permanent
1,          // top_is_horizontal
0,          // tree_is_horizontal
1,          // position_under
0,          // top_more_images_visible
1,          // tree_more_images_visible
"null",     // evaluate_upon_tree_show
"null",     // evaluate_upon_tree_hide
],          //right-to-left
["Documents",			"",	                                                    0,0,1],
["Utilities",			"",		                                                0,0,1],
["Config",		"javascript:void(load_page('CONFIG DOCUMENTALISTA.X'))",1,0,0],
["Change Profile",	    "javascript:void(load_page('OPEN CHANGE PROFILE.X'))",	1,0,0],
["Exit",	            "javascript:void(Close_Window())",	                    1,0,0]
]

HM_Array1_1 = [
[],
["New",			"",			  														    0,0,1],
["Edit",		"javascript:void(load_page('ABRE SELECIONA DOCUMENTO.X'))",			    1,0,0],
["Indexing",	"javascript:void(load_page('ABRE SELECIONA DOCUMENTO INDEXACAO.X'))",	1,0,0],
["Certify",		"javascript:void(load_page('ABRE SELECIONA DOCUMENTO CERTIFICACAO.X'))",1,0,0]
]

HM_Array1_1_1 = [
[],
["WITHOUT Indexing",	"javascript:void(load_page('CRIA NOVO DOCUMENTO.X'))",		        		1,0,0],
["WITH Indexing","javascript:void(load_page('COM INDEXACAO.X'))",						    1,0,0]
]



HM_Array1_2 = [
[],
["Import",			"javascript:void(load_page('ABRE IMPORTACAO.X'))",			             1,0,0],
["Export",			"javascript:void(load_page('ABRE EXPORTACAO.X'))",			             1,0,0],
["Reorganize",		"javascript:void(load_page('ABRE REORGANIZA.X'))",			             1,0,0],
["Reinvert",		"javascript:void(load_page('ABRE REINVERTER.X'))",			             1,0,0],
["Unlock",			"javascript:void(load_page('ABRE DESBLOQUEIA REGISTROS.X'))",            1,0,0],
["Restart Database", "javascript:void(load_page('ABRE REINICIA BD.X'))",				 1,0,0]
]
