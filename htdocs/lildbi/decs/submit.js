 /****************************************************************************
 *                                                                           *
 * Nome do Arquivo: submit.js                                                *
 * Sistema: DeCS Server 1.7                                                  *
 * Funcoes: submete1, submete2 e submete3                                    *
 *                                                                           *
 * Descricao das funcoes:                                                    *
 *                                                                           *
 * submete1(): transfere valores de uma list box para uma ou duas text areas *
 *             que estao num frame do sistema                                *
 * submete2(): transfere valores de varias check boxes para uma ou duas text *
 *             areas que estao num frame do sistema                          *
 * submete3(): transfere os valores que estao no frame do sistema para uma   *
 *             ou duas text area do sistema do cliente                       *
 *                                                                           *
 *  Autor: Joao Rodolfo Suarez de Oliveira                                   *
 *  Data da Ultima Modificacao: 03/08/2001                                   *
 *                                                                           *
 *****************************************************************************/

function submete1(client_textarea, prefixo_descritor)
{

	function checa_candidato(str_termo_candidato)
	//se o candidato a insercao na text area ja'
	//existir, retorna false, caso contrario retorna true
	//Funcao implementada em submete1() e submet2()
	{
		var num_termos = array_termos.length;
		
		//alert("str_termo_candidato: |" + str_termo_candidato + "|");
		/*
		for (var x = 0; x < num_termos; x++)
		{
			if (array_termos[x])
				alert("array_termos[" + x + "]:  |" + array_termos[x] + "|");
		}
		*/
		for (var x = 0; x < num_termos; x++)
		{
			/*
			alert("str_termo_candidato: |" + str_termo_candidato.replace(/\r\n/, "#") + "|");
			alert("array_termos[" + x + "]:  |" + array_termos[x].replace(/\r\n/, "#") + "|");
			alert("CHECA_CANDIDATO(): str_termo_candidato: |" + str_termo_candidato + "|");
			alert("CHECA_CANDIDATO(): array_termos[" + x + "]:  |" + array_termos[x] + "|");
			*/
			if (array_termos[x] == str_termo_candidato)
			{
				//alert("BINGO!!!");
				return false;
			}
		}
		return true;
	}

	var str_aux = "";
	var pattern_port_spanish = /[\s]\[TIPO[\s]DE[\s]PUBLICAC(AO|ION)\]/;
	var pattern_english = /[\s]\[PUBLICATION[\s]TYPE\]/;
	//alert("pattern:  " + pattern);
	
	var str_termo_candidato = "";
	var array_termos  = new Array();
	if (client_textarea.value != "")
	{
		array_termos = client_textarea.value.split(/\r\n/);
	}
	// somente para debug:
	/*
	var num_termos = array_termos.length;
	for (var x = 0; x < num_termos; x++)
	{
		if (array_termos[x])
			alert("array_termos[" + x + "].value:  " + array_termos[x]);
	}
	*/

	
	//var str_aux = new String("");
	//var string_aux = new String("");
	for (i=0; i<document.MyForm.select_words.length; i++)
	{
		if (document.MyForm.select_words.options[i].selected)
		{
			var descritor = new String("");
			//var aux_2 = new String("");
			descritor = document.MyForm.select_words.options[i].value;
			descritor = descritor.toUpperCase();
			//alert("descritor:  " + descritor);
			descritor = descritor.replace(pattern_port_spanish, "");
			descritor = descritor.replace(pattern_english, "");

			//alert("descritor:  |" + descritor + "|");
			str_aux = prefixo_descritor + descritor;
			
			if (checa_candidato(str_aux)) client_textarea.value += str_aux + "\n";
			
			//client_textarea.value += str_aux;
		}
	}
	//alert("client_textarea.value:  " + client_textarea.value);
	return true;
}

function submete2(client_textarea, prefixo_descritor, prefixo_qualificador, from, to, idioma_dos_descritores, flag_t_p)
{

	// flag_t_p eh para sinalizar que a pesquisa no site do DeCS foi feita usando as expressoes _t (tipo de publicacao)
	// e _p (pre-codificado). Nesse caso nao devemos fazer nenhuma mencao a nenhuma check box porque nao ha' qualificadores
	// para esses termos.
	
	
	//alert("client_textarea:  " + client_textarea);
	//alert("client_textarea.value:  " + client_textarea.value);
	//var string_aux = new String("");
	//alert("1");
	
	var pattern_port_spanish = /[\s]\[TIPO[\s]DE[\s]PUBLICAC(AO|ION)\]/;
	var pattern_english = /[\s]\[PUBLICATION[\s]TYPE\]/;
	//alert("pattern:  " + pattern);
	
	function retira_acentos(the_string)
	{
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'A');
		the_string = the_string.replace('�', 'C');
		the_string = the_string.replace('�', 'E');
		the_string = the_string.replace('�', 'E');
		the_string = the_string.replace('�', 'E');
		the_string = the_string.replace('�', 'E');
		the_string = the_string.replace('�', 'I');
		the_string = the_string.replace('�', 'I');
		the_string = the_string.replace('�', 'I');
		the_string = the_string.replace('�', 'I');
		the_string = the_string.replace('�', 'D');
		the_string = the_string.replace('�', 'N');
		the_string = the_string.replace('�', 'O');
		the_string = the_string.replace('�', 'O');
		the_string = the_string.replace('�', 'O');
		the_string = the_string.replace('�', 'O');
		the_string = the_string.replace('�', 'O');
		the_string = the_string.replace('�', 'X');
		the_string = the_string.replace('�', 'O');
		the_string = the_string.replace('�', 'U');
		the_string = the_string.replace('�', 'U');
		the_string = the_string.replace('�', 'U');
		the_string = the_string.replace('�', 'U');
		the_string = the_string.replace('�', 'Y');
		the_string = the_string.replace('�', 'P');
		the_string = the_string.replace('�', 'B');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'a');
		the_string = the_string.replace('�', 'c');
		the_string = the_string.replace('�', 'e');
		the_string = the_string.replace('�', 'e');
		the_string = the_string.replace('�', 'e');
		the_string = the_string.replace('�', 'e');
		the_string = the_string.replace('�', 'i');
		the_string = the_string.replace('�', 'i');
		the_string = the_string.replace('�', 'i');
		the_string = the_string.replace('�', 'i');
		the_string = the_string.replace('�', 'd');
		the_string = the_string.replace('�', 'n');
		the_string = the_string.replace('�', 'o');
		the_string = the_string.replace('�', 'o');
		the_string = the_string.replace('�', 'o');
		the_string = the_string.replace('�', 'o');
		the_string = the_string.replace('�', 'o');
		the_string = the_string.replace('�', 'o');
		the_string = the_string.replace('�', 'u');
		the_string = the_string.replace('�', 'u');
		the_string = the_string.replace('�', 'u');
		the_string = the_string.replace('�', 'u');
		the_string = the_string.replace('�', 'y');
		the_string = the_string.replace('�', 'p');
		the_string = the_string.replace('�', 'y');
		return the_string;
	}
	
	function in_string(sub_string, big_string)
	{
		function substring2(string, start, length) // retorna uma substring
		{
	   		return string.substring(start, start+length);
		}
		if (sub_string.length > big_string.length)
			return false;
		for (i=0; i< big_string.length - sub_string.length + 1; i++)
		{
			if (sub_string == substring2(big_string, i, sub_string.length))
			return true;
		}
		return false;
	}

	function checa_candidato(str_termo_candidato)
	//se o candidato a insercao na text area ja'
	//existir, retorna false, caso contrario retorna true
	//Funcao implementada em submete1() e submet2()
	{
		var num_termos = array_termos.length;
		
		//alert("str_termo_candidato: |" + str_termo_candidato + "|");
		/*
		for (var x = 0; x < num_termos; x++)
		{
			if (array_termos[x])
				alert("array_termos[" + x + "]:  |" + array_termos[x] + "|");
		}
		*/
		for (var x = 0; x < num_termos; x++)
		{
			/*
			alert("str_termo_candidato: |" + str_termo_candidato.replace(/\r\n/, "#") + "|");
			alert("array_termos[" + x + "]:  |" + array_termos[x].replace(/\r\n/, "#") + "|");
			alert("CHECA_CANDIDATO(): str_termo_candidato: |" + str_termo_candidato + "|");
			alert("CHECA_CANDIDATO(): array_termos[" + x + "]:  |" + array_termos[x] + "|");
			*/
			if (array_termos[x] == str_termo_candidato)
			{
				//alert("BINGO!!!");
				return false;
			}
		}
		return true;
	}
	
	function submit_qualifier(j, prefixo_qualificador)
	{
		str_QUALIFIER_array_index = str_DESCRIPTOR_array_index + j;
		//alert("SUBMIT_QUALIFIER(): str_QUALIFIER_array_index: |" + str_QUALIFIER_array_index + "|");
		
		if (document.MyForm.elements[str_QUALIFIER_array_index].checked)
		{
			str_termo_candidato =	prefixo_descritor + 
									document.MyForm.elements[str_DESCRIPTOR_array_index].value + 
									prefixo_qualificador + 
									document.MyForm.elements[str_QUALIFIER_array_index].value;
			
			//Para retira os acentos: retirar o comentario da linha de baixo						
			//str_termo_candidato = retira_acentos(str_termo_candidato);
									
			//alert("SUBMIT_QUALIFIER(): PONTO 1: str_termo_candidato: |" + str_termo_candidato + "|");
 			if (checa_candidato(str_termo_candidato))
			{
				client_textarea.value += str_termo_candidato + "\n";				
				str_QUALIFIER_array_index = "";
			}
		}
		return true;
	}

	function submit_descriptor(i, k, str_dabase_language, prefixo_descritor, prefixo_qualificador)
	{
		boolean_user_has_made_check_for_qualifier = false;
		boolean_aux = false;
		str_DESCRIPTOR_array_index = "checkbox_register" + i + str_dabase_language;

		//client_textarea.value += "i: " + i + "\n";
		//client_textarea.value += "k: " + k + "\n";
		//client_textarea.value += "number_of_qualifiers[k]: " + number_of_qualifiers[k] + "\n";
		//client_textarea.value += "str_DESCRIPTOR_array_index: " + str_DESCRIPTOR_array_index + "\n";
		//client_textarea.value += "document.MyForm.elements[str_DESCRIPTOR_array_index].checked: " + document.MyForm.elements[str_DESCRIPTOR_array_index].checked + "\n";
		//alert("str_DESCRIPTOR_array_index: " + str_DESCRIPTOR_array_index);
		//alert("document.MyForm.elements[str_DESCRIPTOR_array_index].checked: " + document.MyForm.elements[str_DESCRIPTOR_array_index].checked);
		if (document.MyForm.elements[str_DESCRIPTOR_array_index] != null && document.MyForm.elements[str_DESCRIPTOR_array_index].checked)
		{
			if (!boolean_pesquisa_tipo_de_publicacao_e_pre_codificado)
			{
					for (j=1; j <= number_of_qualifiers[k]; j++)
					{
						//alert("SUBMIT_DESCRIPTOR(): ANTES boolean_aux: |" + boolean_aux + "|");
						submit_qualifier(j, prefixo_qualificador);
						boolean_aux = document.MyForm.elements[str_DESCRIPTOR_array_index + j].checked;
						if (boolean_aux)
							boolean_user_has_made_check_for_qualifier = boolean_user_has_made_check_for_qualifier || boolean_aux;
						//alert("SUBMIT_DESCRIPTOR(): DEPOIS boolean_aux: |" + boolean_aux + "|");
					}
					/*
					alert("str_DESCRIPTOR_array_index:  " + str_DESCRIPTOR_array_index);
					alert("boolean_user_has_made_check_for_qualifier:  " + boolean_user_has_made_check_for_qualifier);
					alert("prefixo_descritor:  " + prefixo_descritor);
					alert("document.MyForm.elements[str_DESCRIPTOR_array_index].value:  " + document.MyForm.elements[str_DESCRIPTOR_array_index].value);
					*/
					//alert(":  " + );
			}
			
			if (!boolean_user_has_made_check_for_qualifier)
			{
				//alert("str_DESCRIPTOR_array_index:  " + str_DESCRIPTOR_array_index);
				//alert("boolean_user_has_made_check_for_qualifier:  " + boolean_user_has_made_check_for_qualifier);
				//alert("prefixo_descritor:  " + prefixo_descritor);
				//alert("document.MyForm.elements[str_DESCRIPTOR_array_index].value:  " + document.MyForm.elements[str_DESCRIPTOR_array_index].value);
				//alert("client_textarea.value:  " + client_textarea.value);
				str_termo_candidato =	prefixo_descritor + 
										document.MyForm.elements[str_DESCRIPTOR_array_index].value;
				//alert("PONTO 1: str_termo_candidato: |" + str_termo_candidato + "|");
				//alert("pattern:  " + pattern);
    
				//Para retira os acentos: retirar o comentario da linha de baixo										
				//str_termo_candidato = retira_acentos(str_termo_candidato);
				str_termo_candidato = str_termo_candidato.replace(pattern_port_spanish, "");
				str_termo_candidato = str_termo_candidato.replace(pattern_english, "");
				//alert("PONTO 2: str_termo_candidato: |" + str_termo_candidato + "|");
				if (checa_candidato(str_termo_candidato)) client_textarea.value += str_termo_candidato + "\n";						
			}
		}
		return true;
	}
	
	
	var num_args_da_func = arguments.length;
	var boolean_pesquisa_tipo_de_publicacao_e_pre_codificado = false;
	if (num_args_da_func == 7)
	{
		boolean_pesquisa_tipo_de_publicacao_e_pre_codificado = true;
	}
	
	
	var str_DESCRIPTOR_array_index;
	var str_QUALIFIER_array_index;
	var boolean_user_has_made_check_for_qualifier = false;
	var boolean_aux = false;
	var boolean_language_of_descriptor_i = false;
	var boolean_language_of_descriptor_e = false;
	var boolean_language_of_descriptor_p = false;
	
	var str_termo_candidato = "";
	var array_termos  = new Array();
	if (client_textarea.value != "")
	{
		array_termos = client_textarea.value.split(/\r\n/);
	}
	// somente para debug:
	/*
	var num_termos = array_termos.length;
	for (var x = 0; x < num_termos; x++)
	{
		if (array_termos[x])
			alert("array_termos[" + x + "].value:  " + array_termos[x]);
	}
	*/

	//client_textarea.value += "idioma_dos_descritores: " + idioma_dos_descritores + "\n";
	//client_textarea.value += "boolean_language_of_descriptor_e: " + boolean_language_of_descriptor_e + "\n";
	if (in_string("i", idioma_dos_descritores))
		boolean_language_of_descriptor_i = true;
	if (in_string("e", idioma_dos_descritores))
		boolean_language_of_descriptor_e = true;
	if (in_string("p", idioma_dos_descritores))
		boolean_language_of_descriptor_p = true;
	//client_textarea.value += "boolean_language_of_descriptor_e: " + boolean_language_of_descriptor_e + "\n";

	for (k=0, i=from; i <= to; i++, k++) // para cada registro apresentado
	{
		//client_textarea.value += "from: " + from + "\n";
		//client_textarea.value += "to: " + to + "\n";
		//client_textarea.value += "i: " + i + "\n";
		//client_textarea.value += "k: " + k + "\n";
		// k serve para acessar, dentro da funcao submit_descriptor, o vetor que informa o numero de qualificadores
		if (boolean_language_of_descriptor_i)
			submit_descriptor(i, k, "i", prefixo_descritor, prefixo_qualificador)
		if (boolean_language_of_descriptor_e)
			submit_descriptor(i, k, "e", prefixo_descritor, prefixo_qualificador)
		if (boolean_language_of_descriptor_p)
			submit_descriptor(i, k, "p", prefixo_descritor, prefixo_qualificador)
	}

	/* DEBUG: */
	// client_textarea.value += "from: " + from + "\n";
	// client_textarea.value += "to: " + to + "\n";
	// client_textarea.value += "number_of_qualifiers[0]: " + number_of_qualifiers[0] + "\n";
	// client_textarea.value += "idioma_dos_descritores: " + idioma_dos_descritores + "\n";
	// client_textarea.value += "boolean_language_of_descriptor_i: " + boolean_language_of_descriptor_i + "\n";
	// client_textarea.value += "boolean_language_of_descriptor_e: " + boolean_language_of_descriptor_e + "\n";
	// client_textarea.value += "boolean_language_of_descriptor_p: " + boolean_language_of_descriptor_p + "\n";
	return true;
}

function submete3(client_textarea_1, client_textarea_2)
{
	var num_args_da_func = arguments.length;
	/*
	alert("TEXTAREA 1");
	alert("document.MyForm.TheTextArea_1.value:  " + document.MyForm.TheTextArea_1.value);
	alert("client_textarea_1:  " + client_textarea_1);
	*/
	client_textarea_1.value +=  document.MyForm.TheTextArea_1.value;
	document.MyForm.TheTextArea_1.value = "";
	//alert("client_textarea_1.value:  " + client_textarea_1.value);
	if (num_args_da_func == 2) // se o cliente passou 2 text areas
	{
		/*
		alert("TEXTAREA 2");
		alert("document.MyForm.TheTextArea_2.value:  " + document.MyForm.TheTextArea_2.value);
		alert("client_textarea_2:  " + client_textarea_2);
		*/
		client_textarea_2.value +=  document.MyForm.TheTextArea_2.value;
		document.MyForm.TheTextArea_2.value = "";
		//alert("client_textarea_2.value:  " + client_textarea_2.value);
	}
	/*
	alert("client_textarea_1:  " + client_textarea_1);
	alert("client_textarea_1.value:  " + client_textarea_1.value);
	alert("client_textarea_2:  " + client_textarea_2);
	alert("client_textarea_2.value:  " + client_textarea_2.value);
	*/
	return true;
}

