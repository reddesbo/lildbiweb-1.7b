 /****************************************************************************
 *                                                                           *
 * Nome do Arquivo: decsserver.js                                            *
 * Sistema: DeCS Server 1.7                                                  *
 * Funcoes: close_this_window, send, open_qualifier_tree, close_qualifier,   *
 *          submit_POST_METHOD e submit_GET_METHOD                           *
 *                                                                           *
 * Descricao das funcoes:                                                    *
 *                                                                           *
 * close_this_window(): bla bla bla                                          *
 *             bla bla bla                                                   *
 * send(what_to_do): bla bla                                                 *
 *             bla bla bla                                                   *
 * open_qualifier_tree(language, path_data): bla bla bla                     *
 *             bla bla bla                                                   *
 * close_qualifier(): bla bla bla                                            *
 * submit_POST_METHOD(the_mfn_tree, the_categorie, the_task,                 *
 *                    the_search_exp, the_previous_page): bla bla bla        *
 * submit_GET_METHOD(the_mfn_tree, the_anchor, the_task,                     *
 *                   the_qualifier): bla bla bla                             *
 *                                                                           *
 *  Autor: Joao Rodolfo Suarez de Oliveira                                   *
 *  Data da Ultima Modificacao: 00/00/2001                                   *
 *                                                                           *
 *****************************************************************************/


	var janela_1 = null;
	var janela_2 = null;
	var janela_3 = null;	
	//var path_data = "/decsserver1.6/"
	//var path_cgi = "../cgi-bin" + path_data;
	//alert('path_data:  ' + path_data);
	//alert('path_cgi:  ' + path_cgi);
	//var extensao = ".xis"
	//var extensao = ".xic"

	function close_this_window()
	{ 
		if (window.opener.janela_1 == null)
		{
			return;
		}
		else
			window.opener.janela_1.close();
			
		if (window.opener.janela_1.janela_2 == null)
		{
			return;
		}
		else
			window.opener.janela_1.janela_2.close();
	}

	function send(what_to_do)
	{
		//document.MyForm.IsisScript.value = "../cgi-bin/decsserver1.4/decsserver.xis";
		//alert('what_to_do:  ' + what_to_do);
		switch (what_to_do)
		{

			case 'search':
								document.MyForm.search.value = 'T';
								if (document.MyForm.task.value != null)
										if (document.MyForm.task.value == 'start')
												for (i=0; i<document.MyForm.task_aux1.length; i++)
												{
													if (document.MyForm.elements["task_aux1"][i].checked)
													{
														document.MyForm.task.value = document.MyForm.elements["task_aux1"][i].value;
													}
												};
								break;
			case 'index':
								/*
								alert('document.MyForm.task.value:  ' + document.MyForm.task.value);
								alert('document.MyForm.elements["task_aux2"][0].value:  ' + document.MyForm.elements["task_aux2"][0].value);
								alert('document.MyForm.elements["task_aux2"][1].value:  ' + document.MyForm.elements["task_aux2"][1].value);
								alert('document.MyForm.elements["task_aux2"][2].value:  ' + document.MyForm.elements["task_aux2"][2].value);
								*/
								//alert('document.MyForm.IsisScript.value:  ' + document.MyForm.IsisScript.value);
								document.MyForm.index.value = 'T';
								if (document.MyForm.task.value != null)
										if (document.MyForm.task.value == 'start')
												for (i=0; i<document.MyForm.task_aux2.length; i++)
												{
													if (document.MyForm.elements["task_aux2"][i].checked)
													{
														document.MyForm.task.value = document.MyForm.elements["task_aux2"][i].value;
													}
												};
								break;
			case 'config':
								document.MyForm.config.value = 'T';
								document.MyForm.task.value = what_to_do;
								break;
			case 'apply':
								document.MyForm.apply.value = 'T';
								break;
			case 'newconsult':
								document.MyForm.newconsult.value = 'T';
								break;
			case 'more':
								document.MyForm.more.value = 'T';
								break;
			case 'show':
								document.MyForm.show.value = 'T';
								break;
			case 'tree':
								document.MyForm.tree.value = 'T';
								break;
			case 'back':
								document.MyForm.back.value = 'T';
								break;
			case 'index_aux':
								document.MyForm.index_aux.value = 'T';
								break;
		}		
		document.MyForm.submit();
	}

	function open_qualifier_tree(language, path_data)
	{
		janela_2 = new Object();
		URL = path_data + "qualifier_tree_" + language + ".htm"			
		janela_2 = window.open(URL,"janela_2", "width=450,height=450,scrollbars,resizable,toolbar,status");
	}

	function close_qualifier()
	{ 
		if (janela_1 == null)
		{
			return;
		}
		else
			janela_1.close();
			
		if (janela_1.janela_2 == null)
		{
			return;
		}
		else
			janela_1.janela_2.close();
	}

	function submit_POST_METHOD(the_mfn_tree, the_categorie, the_task, the_search_exp, the_previous_page)
	{
		var num_argumentos_passados_para_a_funcao = arguments.length;
		if (num_argumentos_passados_para_a_funcao > 2) // se a chamada nao foi feita na hierarchic
		{
			//alert(document.MyForm.search_exp[0].value);
			//alert(document.MyForm.search_exp[1].value);
			document.MyForm.task.value = the_task;
			document.MyForm.search_exp.value = the_search_exp;
			//alert(document.MyForm.search_exp[0].value);
			//alert(document.MyForm.search_exp[1].value);
			//alert(document.MyForm.search_exp[2].value);
			document.MyForm.previous_page.value = the_previous_page;
		}
		else // ok, a chamada foi feita dentro da construcao da hierarquia. So' precisamos colocar esses valores:
		{
			if (num_argumentos_passados_para_a_funcao == 2)
			{
				document.MyForm.clear_text_area.value = 'T';
				document.MyForm.categorie.value = the_categorie;
			}
			document.MyForm.mfn_tree.value = the_mfn_tree;
		}
		/*
		if (num_argumentos_passados_para_a_funcao == 6) // se a task for show_qualifier, vamos abrir uma nova janela
		{
			document.MyForm.qualifier.value = the_qualifier;
			janela_1 = new Object();
			janela_1 = window.open("","janela_1", "width=640,height=450,scrollbars,resizable,toolbar,status");				
			document.MyForm.target = "janela_1";
			document.MyForm.submit();
		}
		else
		*/
		document.MyForm.submit();
	}

	function submit_GET_METHOD(the_mfn_tree, the_anchor, the_task, the_qualifier)
	{
		/*
		Essa funcao faz abrir um novo URL na mesma janela.
		O URL pode conter os parametros para uma execucao de
		CGI pelo metodo GET.
		*/
		var num_argumentos_passados_para_a_funcao = arguments.length;
		var URL = "";
		/*
		alert("the_mfn_tree: " + the_mfn_tree + "\n" +  
				"num_argumentos_passados_para_a_funcao: " + num_argumentos_passados_para_a_funcao + "\n" + 
				"document.MyForm.task.value: " + document.MyForm.task.value);
		*/
		/*
		alert("arguments.length: " + arguments.length + "\n" +  
				"arguments[0]: " + arguments[0] + "\n" + 
				"arguments[1]: " + arguments[1] + "\n" + 
				"arguments[2]: " + arguments[2] + "\n" + 
				"arguments[3]: " + arguments[3] + "\n" + 
				"arguments[4]: " + arguments[4] + "\n"
				);
		alert("num_argumentos_passados_para_a_funcao: " + num_argumentos_passados_para_a_funcao);
		alert("document.MyForm.task.value: " + document.MyForm.task.value);
		*/
		// brmg:
		//URL = "http://bvs01.bireme.br/cgi-bin/wxis1660.exe/?IsisScript=" + document.MyForm.IsisScript.value;
		// brmh:
		//URL = "http://brmh.bireme.br/cgi-bin/wxis1660.exe/?IsisScript=" + document.MyForm.IsisScript.value;
		//meu micro:
		//URL = "http://localhost/cgi-bin/wxis1660.exe/?IsisScript=" + document.MyForm.IsisScript.value;
		//testando o URL sem colocar o nome da maquina:
		URL = "/cgi-bin/wxis1660.exe/?IsisScript=" + document.MyForm.IsisScript.value;
		URL += "&path_database=" + document.MyForm.path_database.value;
		URL += "&path_cgibin=" + document.MyForm.path_cgibin.value;
		URL += "&path_data=" + document.MyForm.path_data.value;
		URL += "&temp_dir=" + document.MyForm.temp_dir.value;
		URL += "&debug=" + document.MyForm.debug.value;
		URL += "&clock=" + document.MyForm.clock.value;
		URL += "&client=" + document.MyForm.client.value;
		URL += "&search_language=" + document.MyForm.search_language.value;
		URL += "&interface_language=" + document.MyForm.interface_language.value;
		URL += "&navigation_bar=" + document.MyForm.navigation_bar.value;
		URL += "&format=" + document.MyForm.format.value;
		URL += "&show_tree_number=" + document.MyForm.show_tree_number.value;
		URL += "&list_size=" + document.MyForm.list_size.value;
//alert("document.MyForm.show_tree_number.value: |" + document.MyForm.show_tree_number.value + "|");
		URL += "&from=" + document.MyForm.from.value;
		URL += "&count=" + document.MyForm.count.value;
		URL += "&total=" + document.MyForm.total.value;
		URL += "&no_frame=" + document.MyForm.no_frame.value;		
		if (num_argumentos_passados_para_a_funcao >= 3)
			URL += "&task=" + the_task;
		else
			URL += "&task=" + document.MyForm.task.value;
		URL += "&previous_task=" + document.MyForm.previous_task.value;
		URL += "&previous_page=" + document.MyForm.previous_page.value;
		if (num_argumentos_passados_para_a_funcao == 4) // se a task for show_qualifier, vamos abrir uma nova janela
		{
			URL += "&qualifier=" + the_qualifier;
			//alert("URL: " + URL);
			janela_1 = new Object();
			janela_1 = window.open(URL,"janela_1", "width=640,height=450,scrollbars,resizable,toolbar,status");
			return;
		}
		URL += "&mfn_tree=" + the_mfn_tree;
		if (num_argumentos_passados_para_a_funcao == 1) // se a task for show_qualifier, vamos abrir uma nova janela
		{
			URL += "#Tree" + the_mfn_tree + "-1";
		}
		else
		{
			URL += "#Tree" + the_anchor;
		}
		
//alert("URL: " + URL);
		
		janela_3 = new Object();
		date = new Date();
		var numero;
		// faz atribuicao de quantos milisegundos se passaram
		// desde que comecou o dia
		numero = date.getTime();
		// quero que o nome da janela seja unico,
		// ou seja, nao quero me preocupar se posso
		// estar fazendo referencia a uma janela antiga
		window.name = numero.toString();
		//window.name = window.opener.name;
		//alert("window.opener.name:  " + window.opener.name);
		//alert("window.name:  " + window.name);
		//alert(window.name);
		janela_3 = window.open(URL, window.name);
	}
