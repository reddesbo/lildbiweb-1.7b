 /****************************************************************************
 *                                                                           *
 * Nome do Arquivo: swap_image.js                                            *
 * Sistema: DeCS Server 1.7                                                  *
 * Funcoes: MM_preloadImages(), MM_swapImgRestore(), MM_findObj(n, d) e      *
 *          MM_swapImage()                                                   *
 *                                                                           *
 * Descricao das funcoes:                                                    *
 *                                                                           *
 * Essa funcoes foram criadas automaticamente pelo DreamWeaver da MacroMedia *
 * e servem para fazer a troca de imagens em uma pagina html assim que um    *
 * evento ocorrer.                                                           *
 *                                                                           *
 *  Autor: DreamWeaver da MacroMedia                                         *
 *  Data da Ultima Modificacao: 00/00/2001                                   *
 *                                                                           *
 *****************************************************************************/



	function MM_preloadImages()
	{ // funciona em todos os browsers que tenham versao maior ou igual a 3
		var d = document;
		if (d.images)
		{
			if (!d.MM_p)
				d.MM_p = new Array();
	    var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
		 for (i=0; i<a.length; i++)
	    	if (a[i].indexOf("#") != 0)
			{
				d.MM_p[j] = new Image;
				d.MM_p[j++].src = a[i];
			}
		}
	}
	
	function MM_swapImgRestore()
	{ // funciona em todos os browsers que tenham versao maior ou igual a 3
		var i, x, a = document.MM_sr;
		for (i=0; a && i<a.length && (x=a[i]) && x.oSrc;i++)
			x.src = x.oSrc;
	}
	
	function MM_findObj(n, d)
	{ // funciona em todos os browsers que tenham versao maior ou igual a 3
		var p, i, x;
		if (!d)
		d = document;
		if ( (p = n.indexOf("?")) > 0 && parent.frames.length)
		{
			d = parent.frames[n.substring(p+1)].document;
			n = n.substring(0,p);
		}
		if (!(x=d[n]) && d.all)
			x = d.all[n];
		for (i=0; !x && i < d.forms.length; i++)
			x = d.forms[i][n];
		for (i=0; !x && d.layers && i < d.layers.length; i++)
			x = MM_findObj(n,d.layers[i].document);
		return x;
	}
	
	function MM_swapImage()
	{ // funciona em todos os browsers que tenham versao maior ou igual a 3
		var i, j = 0, x, a = MM_swapImage.arguments;
		document.MM_sr = new Array;
		for (i=0; i < (a.length-2); i += 3)
			if ( (x = MM_findObj(a[i])) != null)
			{
				document.MM_sr[j++] = x;
				if (!x.oSrc)
					x.oSrc=x.src;
				x.src = a[i+2];
			}
	}
