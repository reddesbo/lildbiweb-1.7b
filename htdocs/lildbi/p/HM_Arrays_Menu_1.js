var optWidth = (HM_WindowWidth / 6)-2; // number of menu options
if (navigator.appName == "Netscape")
	var barraSuperior = 48; 
else
	var barraSuperior = 53;

HM_Array1 = [
[
"optWidth",
10,			// left_position
barraSuperior,			// top_position
"black",     // font_color
"white",    // mouseover_font_color
"#779DC1",  // background_color
"#9999CC",  // mouseover_background_color
"black",    // border_color
"black",  // separator_color
1,			// top_is_permanent
1,          // top_is_horizontal
0,          // tree_is_horizontal
1,          // position_under
0,          // top_more_images_visible
1,          // tree_more_images_visible
"null",     // evaluate_upon_tree_show
"null",     // evaluate_upon_tree_hide
],          //right-to-left
["Sistema",		 "",				                                                   0,0,1],
["Usu�rios",	 "javascript:void(load_page('CADASTRO DE USUARIOS.X'))",			   1,0,0],
["Base de Dados","",		                                                           0,0,1],
["Relat�rios",	 "javascript:void(load_page('INFORMACOES GERAIS DA BASE DE DADOS.X'))",1,0,0],
["Trocar Perfil","javascript:void(load_page('OPEN CHANGE PROFILE.X'))",	               1,0,0],
["Sair",         "javascript:void(Close_Window())",     	                           1,0,0]
]

HM_Array1_1 = [
[],
["Disponibilidade",	  "javascript:void(load_page('ABRE DISPONIBILIDADE.X'))",   1,0,0],
["Aviso aos Usu�rios","javascript:void(load_page('AVISO AOS USUARIOS.X'))",     1,0,0]
]

HM_Array1_3 = [
[],
["Defini��es",		  "",	                                                    0,0,1],
["Gerar Invertido",	  "javascript:void(load_page('ABRE GERAR INVERTIDO.X'))",   1,0,0],
["Desbloquear",		  "javascript:void(load_page('MENU DESBLOQUEAR.X'))",		1,0,0],
["Reiniciar Base",	  "javascript:void(load_page('ABRE REINICIA BD.X'))",		1,0,0]
]

HM_Array1_3_1 = [
[],
["Defini��es Gerais", "javascript:void(load_page('DEFINICOES GERAIS.X'))",		     1,0,0],
["Tipos de Registros","javascript:void(load_page('ABRE DEFINIR TIPOS REGISTROS.X'))",1,0,0],
["Complementos",	  "javascript:void(load_page('DEFINIR SUBTIPO DE REGISTRO.X'))", 1,0,0],
["Campos",			  "javascript:void(load_page('DEFINIR CAMPOS.X'))",			     1,0,0],
["FST",				  "javascript:void(load_page('ABRE DEFINE FST.X'))",			 1,0,0],
["Mensagens",		  "javascript:void(load_page('DEFINIR MENSAGENS.X'))",		     1,0,0]
]
