<?php

require ('../lw-upload/functions.php');
require ('../lw-upload/config.php');

$lw_def = parse_ini_file('../lildbi.def.php');
$path_data = $lw_def['PATH_DATA'];

$doc_id = $_GET['id'];
$server = 'http://' . $_SERVER['HTTP_HOST'];

$get_doc_info = $server . '/cgi-bin/wxis1660.exe/' . $path_data . '/upload/?IsisScript=lildbi/upload/upload.xis&id=' .$doc_id .'&step=GET&cipar='.$cipar;

$file_info = file_get_contents($get_doc_info);

$file = $lw_def['PATH_DATABASE'] . 'uploads/' . $file_info;

$file_ext_type = lw_check_filetype( $file );
$filetype = $file_ext_type['type'];

if ( !file_exists($file) || $filetype == false ) {
    header ( $_SERVER["SERVER_PROTOCOL"]." 404 Not Found" );
    print "File not found";
    exit();
}

if ($filetype == 'text/html' || preg_match("/^image/",$filetype) ){
    header("Content-Type: " . $filetype);
    echo @readfile( $file );
    exit();
}

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: $file_type");
header("Content-Disposition: attachment; filename=\"".basename($file)."\";");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".@filesize($file));
set_time_limit(0);

@readfile($file) or die("File not found.");

?>
