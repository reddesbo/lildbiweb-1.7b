<? 

$messageArray = array (
	"es" =>
		array (
			"title" => "$productID: proceso de configuración",
			"msg1" => "Ajustando archivos del sistema ...",
			//"msg2" => "Directório: $documentRoot$pathData",
			"msg2" => "Directório: ",
			"msg3" => "Archivos: ",
			"end"	=> "::Configuración finalizada.",
			"error" => "::Error en el proceso de configuración. Por favor verifique los permisos del directorio y archivos del sistema y ejecute el setup nuevamente.",
			"back" => "Volver",
			"home" => "Página inicial"
		),
	"pt" =>
		array (
			"title" => "$productID: processo de configuração",
			"msg1" => "Ajustando arquivos do sistema ...",
			"msg2" => "Diretório: ",
			"msg3" => "Arquivos: ",
			"end"	=> "::Configuração finalizada.",
			"error" => "::Erro no processo de configuração. Por favor verifique a permissão do diretório e arquivos do sistema e execute o setup novamente.",
			"back" => "Retornar",
			"home" => "Página inicial"
		),
	"en" =>
		array (
			"title" => "$productID: setup process",
			"msg1" => "Adjusting system files ...",
			"msg2" => "Path: ",
			"msg3" => "Files: :",
			"end"	=> "::Setup complete.",
			"error" => "::Error in setup process. Please check permission of system files and directory and execute setup.",
			"back" => "Back",
			"home" => "Home",
		),
);

?>
