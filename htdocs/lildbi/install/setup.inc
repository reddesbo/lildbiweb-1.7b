<? 
$productID = "BVS-LILACS";
$messageArray = array (
	"es" =>
		array (
			"title" => "$productID: configuraci�n",
			"para1" => "<p>El Setup encontr� la informaci�n necesaria para instalar <b>$productID</b>.<br>Lea atentamente la siguiente informaci�n y alt�rela si es necesario.</p>",
			"server" => "Nombre del servidor: ",
			"server_path" => "Directorio ra�z: ",
            "para2" => "<br><p>Caminos para los directorios donde los archivos del <b>$productID</b> fueron instalados.</p>",
			"htdocs" => ". documentos: ",
			"cgi_path" => ". cgi-bin: ",
			"db_path" => ". bases: ",
			"para3" => "<p>Al presionar el bot�n \"Pr�ximo\" el Setup empezar� a procesar los archivos de la aplicaci�n basandose en la informaci�n descrita.</p>",
			"apply" => "Pr�ximo",
			"obs1" => "no es necesario alterar",
			"obs2" => "confirma/altera",
			"language" => "<a href='?lang=pt'>portugu�s</a>  |  <a href='?lang=en'>english</a>"
		),
	"pt" =>
		array (
			"title" => "$productID: configura��o",
			"para1" => "<p>O Setup encontrou a informa��o necess�ria para instalar <b>$productID</b>.<br>Leia atentamente a informa��o abaixo e altere-a se necess�rio.</p>",
			"server" => "Nome do servidor: ",
			"server_path" => "Diret�rio ra�z: ",
            "para2" => "<br><p>Caminhos para os diret�rios onde os arquivos de <b>$productID</b> foram instalados.</p>",
			"htdocs" => ". documentos: ",
			"cgi_path" => ". cgi-bin: ",
			"db_path" => ". bases: ",
			"para3" => "<p>Ao pressionar o bot�o \"Pr�ximo\" o Setup come�ar� a processar os arquivos da aplica��o baseando-se na informa��o acima.</p>",
			"apply" => "Pr�ximo",
			"obs1" => "n�o � necess�rio alterar",
			"obs2" => "confirma/altera",
			"language" => "<a href='?lang=es'>espa�ol</a>  |  <a href='?lang=en'>english</a>"
		),
	"en" =>
		array (
			"title" => "$productID: setup",
			"para1" => "<p>The setup has found the information required to install <b>$productID</b>.<br>Read carefully the information below and change it whenever necessary.</p>",
			"server" => "Servername: ",
			"server_path" => "Root directory: ",
            "para2" => "<br><p>Paths to directories where <b>$productID</b> files have been installed.</p>",
			"htdocs" => ". documents: ",
			"cgi_path" => ". cgi-bin: ",
			"db_path" => ". bases: ",
			"para3" => "<p>By pressing the \"Next\" button Setup will start processing application files based on the information given above.</p>",
			"apply" => "Next",
			"obs1" => "do not need changing",
			"obs2" => "confirm/change",
			"language" => "<a href='?lang=pt'>portugu�s</a>  |  <a href='?lang=es'>espa�ol</a>"
		),
);
?>
