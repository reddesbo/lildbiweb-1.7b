var optWidth = (HM_WindowWidth / 5)-2; // number of menu options
if (navigator.appName == "Netscape")
	var barraSuperior = 48; 
else
	var barraSuperior = 53;

HM_Array1 = [
[
optWidth, // menu width
10,			// left_position
barraSuperior,			// top_position
"black",     // font_color
"white",    // mouseover_font_color
"#779DC1",  // background_color
"#9999CC",  // mouseover_background_color
"black",    // border_color
"black",  // separator_color
1,			// top_is_permanent
1,          // top_is_horizontal
0,          // tree_is_horizontal
1,          // position_under
0,          // top_more_images_visible
1,          // tree_more_images_visible
"null",     // evaluate_upon_tree_show
"null",     // evaluate_upon_tree_hide
],          //right-to-left
["Documentos",			"",	                                                    0,0,1],
["Utilitarios",			"",		                                                0,0,1],
["Configuración",		"javascript:void(load_page('CONFIG DOCUMENTALISTA.X'))",1,0,0],
["Cambiar Perfil",	    "javascript:void(load_page('OPEN CHANGE PROFILE.X'))",	1,0,0],
["Salir",	            "javascript:void(Close_Window())",	                    1,0,0]
]

HM_Array1_1 = [
[],
["Nuevo",			"",			  														    0,0,1],
["Editar",			"javascript:void(load_page('ABRE SELECIONA DOCUMENTO.X'))",			    1,0,0],
["Indizar",			"javascript:void(load_page('ABRE SELECIONA DOCUMENTO INDEXACAO.X'))",	1,0,0],
["Certificar",		"javascript:void(load_page('ABRE SELECIONA DOCUMENTO CERTIFICACAO.X'))",1,0,0]
]

HM_Array1_1_1 = [
[],
["SIN Indización",	"javascript:void(load_page('CRIA NOVO DOCUMENTO.X'))",		        		1,0,0],
["CON Indización",	"javascript:void(load_page('COM INDEXACAO.X'))",						    1,0,0]
]


HM_Array1_2 = [
[],
["Importar",			"javascript:void(load_page('ABRE IMPORTACAO.X'))",			             1,0,0],
["Exportar",			"javascript:void(load_page('ABRE EXPORTACAO.X'))",			             1,0,0],
["Reorganizar",			"javascript:void(load_page('ABRE REORGANIZA.X'))",			             1,0,0],
["Reinvertir",			"javascript:void(load_page('ABRE REINVERTER.X'))",			             1,0,0],
["Desbloquear",			"javascript:void(load_page('ABRE DESBLOQUEIA REGISTROS.X'))",            1,0,0],
["Reiniciar Base",	    "javascript:void(load_page('ABRE REINICIA BD.X'))",						 1,0,0]
]
