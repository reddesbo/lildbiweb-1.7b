<?php
/**
 * Handle Upload Files
 *
 * @param array $file Reference to a single element of $_FILES. Call the function once for each uploaded file.
 * @return array On success, returns an associative array of file attributes. On failure, returns $overrides['upload_error_handler'](&$file, $message ) or array( 'error'=>$message ).
 */
function lw_handle_upload( &$file, $upload_path = null, $mimes = false, $subdir = null ) {

	// Courtesy of php.net, the strings that describe the error indicated in $_FILES[{form field}]['error'].
	$upload_error_strings = array( false,
		'upload_max_filesize',
		'max_file_size',
		'partially',
		'no_file_was_uploaded',
		'',
		'missing_temporary_folder',
		'failed_write_file',
		'stopped_by_extension');

    $test_form = true;
	$test_size = true;
	$test_upload = true;
    
	// If you override this, you must provide $ext and $type!!!!
	$test_type = true;

	// A successful upload will pass this test. It makes no sense to override this one.
	if ( $file['error'] > 0 )
        return array( 'error' => $upload_error_strings[$file['error']] );

	// A non-empty file will pass this test.
	if ( $test_size && !($file['size'] > 0 ) ) {
		$error_msg = 'file_is_empty';
        return array( 'error' => $error_msg );
	}

	// A properly uploaded file will pass this test. There should be no reason to override this one.
	if ( $test_upload && ! @ is_uploaded_file( $file['tmp_name'] ) )
        return array( 'error' => 'failed_upload_test' );

	// A correct MIME type will pass this test. Override $mimes or use the upload_mimes filter.
	if ( $test_type ) {
        $lw_filetype = lw_check_filetype( $file['name'], $mimes );
        extract( $lw_filetype );

		// Check to see if lw_check_filetype_and_ext() determined the filename was incorrect
		if ( $proper_filename )
			$file['name'] = $proper_filename;

		if ( ( !$type || !$ext ) )
            return array( 'error' => 'security_guidelines' );

		if ( !$ext )
			$ext = ltrim(strrchr($file['name'], '.'), '.');

		if ( !$type )
			$type = $file['type'];
	} else {
		$type = '';
	}

	// A writable uploads dir will pass this test. Again, there's no point overriding this one.
	if ( ! ( ( $uploads = lw_upload_dir($time, $upload_path, $subdir) ) && false === $uploads['error'] ) )
        return array( 'error' => $uploads['error'] );

	$filename = lw_unique_filename( $uploads['path'], $file['name'] );

	// Move the file to the uploads dir
	$new_file = $uploads['path'] . "/$filename";
	if ( false === @ move_uploaded_file( $file['tmp_name'], $new_file ) )
        return array( 'error' => 'fail_move_upload_directory' );

	// Set correct file permissions
	$stat = stat( dirname( $new_file ));
	$perms = $stat['mode'] & 0000666;
	@ chmod( $new_file, $perms );

	// Compute the URL
	$url = $uploads['url'] . "/$filename";

	return array( 'file' => $new_file, 'url' => $url, 'type' => $type );
}

/**
 * Retrieve the file type from the file name.
 *
 * You can optionally define the mime array, if needed.
 *
 *
 * @param string $filename File name or path.
 * @param array $mimes Optional. Key is the file extension with value as the mime type.
 * @return array Values with extension first and mime type.
 */
function lw_check_filetype( $filename, $mimes = null ) {
	if ( empty($mimes) )
		$mimes = get_allowed_mime_types();
	$type = false;
	$ext = false;

	foreach ( $mimes as $ext_preg => $mime_match ) {
		$ext_preg = '!\.(' . $ext_preg . ')$!i';
		if ( preg_match( $ext_preg, $filename, $ext_matches ) ) {
			$type = $mime_match;
			$ext = $ext_matches[1];
			break;
		}
	}

	return compact( 'ext', 'type' );
}


/**
 * Retrieve list of allowed mime types and file extensions.
 *
 * @return array Array of mime types keyed by the file extension regex corresponding to those types.
 */
function get_allowed_mime_types() {
	static $mimes = false;

	if ( !$mimes ) {
		// Accepted MIME types are set here as PCRE unless provided.
		$mimes = array(
		'jpg|jpeg|jpe' => 'image/jpeg',
		'gif' => 'image/gif',
		'png' => 'image/png',
		'bmp' => 'image/bmp',
		'tif|tiff' => 'image/tiff',
		'ico' => 'image/x-icon',
		'asf|asx|wax|wmv|wmx' => 'video/asf',
		'avi' => 'video/avi',
		'divx' => 'video/divx',
		'flv' => 'video/x-flv',
		'mov|qt' => 'video/quicktime',
		'mpeg|mpg|mpe' => 'video/mpeg',
		'txt|asc' => 'text/plain',
		'csv' => 'text/csv',
		'tsv' => 'text/tab-separated-values',
		'rtx' => 'text/richtext',
		'css' => 'text/css',
		'htm|html' => 'text/html',
		'mp3|m4a|m4b' => 'audio/mpeg',
		'mp4|m4v' => 'video/mp4',
		'ra|ram' => 'audio/x-realaudio',
		'wav' => 'audio/wav',
		'ogg|oga' => 'audio/ogg',
		'ogv' => 'video/ogg',
		'mid|midi' => 'audio/midi',
		'wma' => 'audio/wma',
		'mka' => 'audio/x-matroska',
		'mkv' => 'video/x-matroska',
		'rtf' => 'application/rtf',
		'pdf' => 'application/pdf',
		'doc|docx' => 'application/msword',
		'pot|pps|ppt|pptx|ppam|pptm|sldm|ppsm|potm' => 'application/vnd.ms-powerpoint',
		'wri' => 'application/vnd.ms-write',
		'xla|xls|xlsx|xlt|xlw|xlam|xlsb|xlsm|xltm' => 'application/vnd.ms-excel',
		'mdb' => 'application/vnd.ms-access',
		'mpp' => 'application/vnd.ms-project',
		'docm|dotm' => 'application/vnd.ms-word',
		'pptx|sldx|ppsx|potx' => 'application/vnd.openxmlformats-officedocument.presentationml',
		'xlsx|xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml',
		'docx|dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml',
		'onetoc|onetoc2|onetmp|onepkg' => 'application/onenote',
		'swf' => 'application/x-shockwave-flash',
		'tar' => 'application/x-tar',
		'zip' => 'application/zip',
		'gz|gzip' => 'application/x-gzip',
		// openoffice formats
		'odt' => 'application/vnd.oasis.opendocument.text',
		'odp' => 'application/vnd.oasis.opendocument.presentation',
		'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		'odg' => 'application/vnd.oasis.opendocument.graphics',
		'odc' => 'application/vnd.oasis.opendocument.chart',
		'odb' => 'application/vnd.oasis.opendocument.database',
		'odf' => 'application/vnd.oasis.opendocument.formula',
		// iso 2709
		'iso' => 'text/iso',
		);
	}

	return $mimes;
}

/**
 * Get an array containing the current upload directory's path and url.
 *
 * Checks the 'upload_path' option, which should be from the web root folder,
 * and if it isn't empty it will be used. If it is empty, then the path will be
 * 'LW_CONTENT_DIR/uploads'. If the 'UPLOADS' constant is defined, then it will
 * override the 'upload_path' option and 'LW_CONTENT_DIR/uploads' path.
 *
 * The upload URL path is set either by the 'upload_url_path' option or by using
 * the 'LW_CONTENT_URL' constant and appending '/uploads' to the path.
 *
 * If the 'uploads_use_yearmonth_folders' is set to true (checkbox if checked in
 * the administration settings panel), then the time will be used. The format
 * will be year first and then month.
 *
 * If the path couldn't be created, then an error will be returned with the key
 * 'error' containing the error message. The error suggests that the parent
 * directory is not writable by the server.
 *
 * On success, the returned array will have many indices:
 * 'path' - base directory and sub directory or full path to upload directory.
 * 'url' - base url and sub directory or absolute URL to upload directory.
 * 'subdir' - sub directory if uploads use year/month folders option is on.
 * 'basedir' - path without subdir.
 * 'baseurl' - URL path without subdir.
 * 'error' - set to false.
 *
 * @param string $time Optional. Time formatted in 'yyyy/mm'.
 * @return array See above for description.
 */
function lw_upload_dir( $time = null, $upload_path = null, $subdir = null ) {
	$siteurl = $siteurl;
	$upload_path = trim($upload_path);
	$main_override = false;
	if ( empty($upload_path) ) {
		$dir = '/uploads';
	} else {
		$dir = $upload_path;
	}

	$bdir = $dir;
	$burl = $url;

    if ( $subdir == null ){
        $subdir = '';
        // Generate the yearly and monthly dirs
        if ( !$time )
            $time = date('Ym');
        $y = substr( $time, 0, 4 );
        $m = substr( $time, 4, 2 );
        $subdir = "/$y/$m";
    }    

	$dir .= $subdir;
	$url .= $subdir;

	$uploads = array( 'path' => $dir, 'url' => $url, 'subdir' => $subdir, 'basedir' => $bdir, 'baseurl' => $burl, 'error' => false );

	// Make sure we have an uploads dir
	if ( ! lw_mkdir_p( $uploads['path'] ) ) {
		return array( 'error' => 'unable_create_directory' );
	}

	return $uploads;
}

/**
 * Recursive directory creation based on full path.
 *
 * Will attempt to set permissions on folders.
 *
 * @param string $target Full path to attempt to create.
 * @return bool Whether the path was created. True if path already exists.
 */
function lw_mkdir_p( $target ) {
	// from php.net/mkdir user contributed notes
	$target = str_replace( '//', '/', $target );

	// safe mode fails with a trailing slash under certain PHP versions.
	$target = rtrim($target, '/'); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
	if ( empty($target) )
		$target = '/';

	if ( file_exists( $target ) )
		return @is_dir( $target );

	// Attempting to create the directory may clutter up our display.
	if ( @mkdir( $target ) ) {
		$stat = @stat( dirname( $target ) );
		$dir_perms = $stat['mode'] & 0007777;  // Get the permission bits.
		@chmod( $target, $dir_perms );
		return true;
	} elseif ( is_dir( dirname( $target ) ) ) {
			return false;
	}

	// If the above failed, attempt to create the parent node, then try again.
	if ( ( $target != '/' ) && ( lw_mkdir_p( dirname( $target ) ) ) )
		return lw_mkdir_p( $target );

	return false;
}


/**
 * Get a filename that is sanitized and unique for the given directory.
 *
 * If the filename is not unique, then a number will be added to the filename
 * before the extension, and will continue adding numbers until the filename is
 * unique.
 *
 * @param string $dir
 * @param string $filename
 * @param string $unique_filename_callback Function name, must be a function.
 * @return string New filename, if given wasn't unique.
 */
function lw_unique_filename( $dir, $filename ) {
	// sanitize the file name before we begin processing
	$filename = sanitize_file_name($filename);

	// separate the filename into a name and extension
	$info = pathinfo($filename);
	$ext = !empty($info['extension']) ? '.' . $info['extension'] : '';
	$name = basename($filename, $ext);

	// edge case: if file is named '.ext', treat as an empty name
	if ( $name === $ext )
		$name = '';

	// Increment the file number until we have a unique file to save in $dir. Use $override['unique_filename_callback'] if supplied.
		$number = '';

    // change '.ext' to lower case
    if ( $ext && strtolower($ext) != $ext ) {
        $ext2 = strtolower($ext);
        $filename2 = preg_replace( '|' . preg_quote($ext) . '$|', $ext2, $filename );

        // check for both lower and upper case extension or image sub-sizes may be overwritten
        while ( file_exists($dir . "/$filename") || file_exists($dir . "/$filename2") ) {
            $new_number = $number + 1;
            $filename = str_replace( "$number$ext", "$new_number$ext", $filename );
            $filename2 = str_replace( "$number$ext2", "$new_number$ext2", $filename2 );
            $number = $new_number;
        }
        return $filename2;
    }

    while ( file_exists( $dir . "/$filename" ) ) {
        if ( '' == "$number$ext" )
            $filename = $filename . ++$number . $ext;
        else
            $filename = str_replace( "$number$ext", ++$number . $ext, $filename );
    }
	

	return $filename;
}

/**
 * Sanitizes a filename replacing whitespace with dashes
 *
 * Removes special characters that are illegal in filenames on certain
 * operating systems and special characters requiring special escaping
 * to manipulate at the command line. Replaces spaces and consecutive
 * dashes with a single dash. Trim period, dash and underscore from beginning
 * and end of filename.
 *
 *
 * @param string $filename The filename to be sanitized
 * @return string The sanitized filename
 */
function sanitize_file_name( $filename ) {
	$filename_raw = $filename;
	$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
	$filename = str_replace($special_chars, '', $filename);
	$filename = preg_replace('/[\s-]+/', '-', $filename);
	$filename = trim($filename, '.-_');

	// Split the filename into a base and extension[s]
	$parts = explode('.', $filename);

	// Process multiple extensions
	$filename = array_shift($parts);
	$extension = array_pop($parts);
	$mimes = get_allowed_mime_types();

	// Loop over any intermediate extensions.  Munge them with a trailing underscore if they are a 2 - 5 character
	// long alpha string not in the extension whitelist.
	foreach ( (array) $parts as $part) {
		$filename .= '.' . $part;

		if ( preg_match("/^[a-zA-Z]{2,5}\d?$/", $part) ) {
			$allowed = false;
			foreach ( $mimes as $ext_preg => $mime_match ) {
				$ext_preg = '!(^' . $ext_preg . ')$!i';
				if ( preg_match( $ext_preg, $part ) ) {
					$allowed = true;
					break;
				}
			}
			if ( !$allowed )
				$filename .= '_';
		}
	}
	$filename .= '.' . $extension;

	return $filename;
}

/**
 * Check HTTP_REFERER
 *
 */ 
function check_admin_referer($adminurl) {
    $result = false;
    $adminurl = strtolower($adminurl);
    $self_script_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
    $referer = strtolower($_SERVER['HTTP_REFERER']);
    
    // pass if referer = adminurl or self_url
    if ( preg_match ('@^'. $adminurl . '@', $referer) || preg_match ('@^'. $self_script_url . '@', $referer) )
        $result = true;

    return $result;
}

function lw_max_upload_size() {
	$u_bytes = lw_convert_hr_to_bytes( ini_get( 'upload_max_filesize' ) );
	$p_bytes = lw_convert_hr_to_bytes( ini_get( 'post_max_size' ) );
	$bytes =  min($u_bytes, $p_bytes);
    
	return $bytes;
}

function lw_convert_hr_to_bytes( $size ) {
	$size = strtolower($size);
	$bytes = (int) $size;
	if ( strpos($size, 'k') !== false )
		$bytes = intval($size) * 1024;
	elseif ( strpos($size, 'm') !== false )
		$bytes = intval($size) * 1024 * 1024;
	elseif ( strpos($size, 'g') !== false )
		$bytes = intval($size) * 1024 * 1024 * 1024;
	return lw_byte_size($bytes);
}

function lw_byte_size($bytes){ 
    $size = $bytes / 1024; 
    if($size < 1024) { 
        $size = number_format($size, 2); 
        $size .= ' KB'; 
    } else { 
        if($size / 1024 < 1024) { 
            $size = number_format($size / 1024, 2); 
            $size .= ' MB'; 
        } else if ($size / 1024 / 1024 < 1024) { 
            $size = number_format($size / 1024 / 1024, 2); 
            $size .= ' GB'; 
        }  
    } 

    return $size; 
} 

?>
