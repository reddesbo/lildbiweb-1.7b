<?php
session_start();

require ('mkdir.php');
require ('functions.php');
require ( 'texts.php');

function writeFile($s_file, $i_code, $content) {
    createDirectory(dirname($s_file), $i_code);
    $fp = fopen($s_file, "a+");
    fwrite($fp, $content);
    fclose($fp);
}

function DeleteFile ($f) {
    if (is_file($f)) {
        unlink ($f);
    }
}

$lw_def = parse_ini_file('../lildbi.def.php');
$docUploadDir = $lw_def['PATH_DATABASE'] . 'uploads/';
$lw_admin_url = 'http://' . $_SERVER['HTTP_HOST'] . '/cgi-bin/wxis1660.exe' . $lw_def['PATH_DATA'] . 'upload/';

$date = date("YmdHi");
$procFile = $_REQUEST['procFile'] . $date;

$script = $_REQUEST['script'];
$IsisScript = $_REQUEST['IsisScript'];
$userControl = $_REQUEST['userControl'];
$lang = $_REQUEST['lang'];
$cipar = $_REQUEST['cipar'];
$token = $_REQUEST['token'];
$step = $_REQUEST['step'];
$tag = $_REQUEST['tag'];
$myFullText = $_POST['myFullText'];
$op = $_REQUEST['op'];
$mfn = $_REQUEST['mfn'];
$docId = $_REQUEST['docId'];
$docRelPath = $_REQUEST['docRelPath'];
$docFileName = $_REQUEST['docFileName'];
$docFormerFileName = $_REQUEST['docFormerFileName'];
$file = $_FILES['myFile'];

//check referer, token, required variables
if ( !check_admin_referer($lw_admin_url) || $_REQUEST['token'] != $_SESSION['token'] 
    || !isset($script) || !isset($IsisScript) || !isset($userControl) || !isset($step) || !isset($docId) || !isset($op) ){
        
    header ( $_SERVER["SERVER_PROTOCOL"]." 403 Forbidden" );
    print "Forbidden";
    exit();
}

$k = 0;

$delete = ($op[$k]=='delete');
$upload = false;

if ( isset($tag) ) { 
    foreach ($tag as $t) {
        $procDelete .= 'd'.$t;
    }
}    

if ($file) {
    if ($file != 'none') {
        $upload = true;
        if ($docFormerFileName[$k] && ($docFormerFileName[$k] != basename($file[0]['name']))) {
            $delete = true;
        }
    }
}
if ($delete) {
    DeleteFile($docUploadDir.$docRelPath[$k].$docFileName[$k]);
}

if ($upload) {
    
    // create single file info
    foreach( $file as $key=>$value) {        
        $upload_file[$key] = $value[0];
    }
    
    $upload_status = lw_handle_upload( $upload_file, $docUploadDir, null, $docRelPath[$k] );
  
    $docFileName[$k] = sanitize_file_name( $upload_file['name'] );
   
    if ( !$upload_status['error'] ) {
        $uploaded = true;
    }else{
        $status_message = $upload_status['error'];
        echo '<html><body bgcolor="#CAD7E6">';
        echo '<h1>' . $texts[$lang][$status_message] . '</h1>';
        echo '<form><input type="button" class="button" value="' . $texts[$lang]['back'] . '" onclick="history.back();"></form>';
        echo '</body></html>';
        exit();        
    }

}

$docFormerFileName[$k] = $file['name'][0];

$i=0;

$content[$i++] = $date; 
$content[$i++] = $docId[$k]; 
$content[$i++] = $docRoot[$k]; 
$content[$i++] = $docRelPath[$k]; 
$content[$i++] = $docFileName[$k]; 
$content[$i++] = $docFormerFileName[$k]; 
$content[$i++] = $docRootSubdir[$k]; 

$operation = 'Cancel';
if ($upload && !$uploaded) $operation = 'WriteError';
if ($delete) $operation = 'Delete';
if ($upload) $operation = 'Save';

$proc = '^a'.$userControl.'^b'.$mfn[$k].'^c'.$operation.'^e'.$button.'^d'.$procDelete;

$i=0;
if ($upload) {
    foreach ($tag as $t) {
        $proc .= 'a'.$t.'{'.$content[$i++].'{';
    }

    if (strlen($myFullText[$k])>0) {
        $tag++;
        $proc.='a999{'.$myFullText[$k].'{';
    }
}

writeFile ($procFile,0755,"$proc\n");


echo '
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>' . time() . '</title>
    </head>';

echo '<body onLoad="document.continueformUpload.submit();">';
echo '
        <form name="continueformUpload" action="'.$script.'" method="post">
            <input type="hidden" name="lang" value="'.$lang.'"/>
            <input type="hidden" name="procFile" value="'.$procFile.'"/>
            <input type="hidden" name="IsisScript" value="'.$IsisScript.'"/>
            <input type="hidden" name="step" value="'.$step.'"/>
            <input type="hidden" name="cipar" value="'.$cipar.'"/>
            
        </form>
    </body>
</html>';

?>

