<?php

$texts['en']['file'] = "File";
$texts['en']['upload_max_filesize'] = "The uploaded file exceeds the <code>upload_max_filesize</code> directive in <code>php.ini</code>.";
$texts['en']['max_file_size'] = "The uploaded file exceeds the <em>MAX_FILE_SIZE</em> directive that was specified in the HTML form.";
$texts['en']['partially'] = "The uploaded file was only partially uploaded.";
$texts['en']['no_file_was_uploaded'] = "No file was uploaded.";
$texts['en']['missing_temporary_folder'] = "Missing a temporary folder.";
$texts['en']['failed_write_file'] = "Failed to write file to disk.";
$texts['en']['stopped_by_extension'] = "File upload stopped by extension.";
$texts['en']['invalid_form_submission'] = "Invalid form submission.'";
$texts['en']['file_is_empty'] = "File is empty. Please upload something more substantial. This error could also be caused by uploads being disabled in your php.ini or by post_max_size being defined as smaller than upload_max_filesize in php.ini.";
$texts['en']['failed_upload_test'] = "Specified file failed upload test.";
$texts['en']['security_guidelines'] = "File type does not meet security guidelines. Try another.";
$texts['en']['fail_move_upload_directory'] = "The uploaded file could not be moved to upload directory";
$texts['en']['unable_create_directory'] = "Unable to create directory. Is its parent directory writable by the server?";
$texts['en']['max_allowed_file_size'] = "Maximum file size";
$texts['en']['send'] = "Upload";
$texts['en']['back'] = "Back";


$texts['pt']['file'] = "Arquivo";
$texts['pt']['upload_max_filesize'] = "O arquivo carregado excede o par�metro <code>upload_max_filesize</code> no arquivo <code>php.ini</code>.";
$texts['pt']['max_file_size'] = "O arquivo carregado excede o par�metro <em>MAX_FILE_SIZE</em> especificado no formul�rio HTML.";
$texts['pt']['partially'] = "O arquivo foi apenas parcialmente carregado.";
$texts['pt']['no_file_was_uploaded'] = "Nenhum arquivo foi enviado.";
$texts['pt']['missing_temporary_folder'] = "Diret�rio tempor�rio n�o encontrado.";
$texts['pt']['failed_write_file'] = "Falha ao escrever arquivo em disco.";
$texts['pt']['stopped_by_extension'] = "Upload interrompido pela extens�o do arquivo.";
$texts['pt']['invalid_form_submission'] = "Envio inv�lido do formul�rio.'";
$texts['pt']['file_is_empty'] = "O arquivo � vazio. Fa�a upload de algo mais substancial. Este erro tamb�m pode ser causada por uploads ser desativado no seu php.ini ou post_max_size ser definida como inferior a upload_max_filesize no php.ini.";
$texts['pt']['failed_upload_test'] = "O arquivo especificado falhou no teste de upload.";
$texts['pt']['security_guidelines'] = "O tipo do arquivo n�o atende as regras de seguran�a. Tente outro.";
$texts['pt']['fail_move_upload_directory'] = "O arquivo enviado n�o p�de ser movido para o diret�rio de upload do sistema. Verifique as permiss�es de grava��o.";
$texts['pt']['unable_create_directory'] = "N�o foi possivel criar o diret�rio. Verifique as permiss�es no servidor.";
$texts['pt']['max_allowed_file_size'] = "O tamanho m�ximo";
$texts['pt']['send'] = "Enviar";
$texts['pt']['back'] = "Voltar";

$texts['es']['file'] = "Archivo";
$texts['es']['upload_max_filesize'] = "El archivo cargado excede el parametro <code>upload_max_filesize</code> en <code>php.ini</code>.";
$texts['es']['max_file_size'] = "The archivo cargado excede el parametro <em>MAX_FILE_SIZE</em> especificado en el formulario HTML.";
$texts['es']['partially'] = "El archivo fue cargado parcialmente.";
$texts['es']['no_file_was_uploaded'] = "Ning�n archivo fue cargado.";
$texts['es']['missing_temporary_folder'] = "Directorio tempor�rio no encontrado.";
$texts['es']['failed_write_file'] = "No se pudo escribir el archivo en el disco.";
$texts['es']['stopped_by_extension'] = "Carga del archivo se detuvo por la extensi�n.";
$texts['es']['invalid_form_submission'] = "Envio invalido del formulario.'";
$texts['es']['file_is_empty'] = "El archivo est� vacio.  Por favor, sube algo m�s sustancial. Este error tambi�n puede ser causado por upload no estar activado en el php.ini o por post_max_size se define como menor que upload_max_filesize en php.ini.";
$texts['es']['failed_upload_test'] = "El archivo fallo en la prueba de upload.";
$texts['es']['security_guidelines'] = "Tipo de archivo no cumple con las directrices de seguridad. Pruebe con otro.";
$texts['es']['fail_move_upload_directory'] = "El archivo cargado no se pudo mover al directorio de uploads del sistema. Verifique los permisos.";
$texts['es']['unable_create_directory'] = "No se puede crear el directorio. Verifique los permisos en el server";
$texts['es']['max_allowed_file_size'] = "Tama�o m�ximo del archivo";
$texts['es']['send'] = "Cargar";
$texts['es']['back'] = "Volver";

?>
