<?php

session_start();

ini_set("error_reporting", E_PARSE|E_ERROR|E_WARNING);    //Enable errors, but not basic them
ini_set("register_globals", "off");

require( 'functions.php' );
require( 'texts.php');

$lang = ( isset($_POST['lang']) ? $_POST['lang'] : 'pt');

$lw_def = parse_ini_file('../lildbi.def.php');
$lw_admin_url = 'http://' . $_SERVER['HTTP_HOST'] . '/cgi-bin/wxis1660.exe' . $lw_def['PATH_DATA'];
$upload_path = $lw_def['PATH_DATABASE'] . 'uploads/';

// TODO: check for lildbi admin cookie

if ( !check_admin_referer($lw_admin_url) ){
    header("Status: 404 Not Found");
    exit();
}

?>
<html>
<head>

<script language="javascript">
    function continuar(file) {      
        window.opener.document.forms[0].TBArquivoISO.value = file;
        window.close();
    }
</script>

<style>
    body { padding: 10px; background-color: #CAD7E6; font-family: 'Lucida Grande', Verdana; font-size: 13px; color: #333; }
    input { background-color: white; border-color: #DFDFDF; border: 1px solid #2683AE; padding: 2px;  }
    .button { font-size: 12px; border-color: #BBB; color: #464646; background-color:  #2683AE; border: 1px solid #2683AE; color: #fff; padding: 2px 10px;  -moz-border-radius:11px;-khtml-border-radius:11px;-webkit-border-radius:11px;border-radius:11px;cursor:pointer;text-decoration:none;margin-top:-3px; font-weight: bold;}
    .upload-box { padding: 20px; background-color: #F2F2F2; }
</style>

</head>

<body>
    
<?php   
    $max_upload_size = lw_max_upload_size();

    if ( isset($_POST['step']) && $_POST['step'] != '' ) {

        if ( isset($_SESSION['token']) && $_POST['token'] == $_SESSION['token'] ) {

            // set mime to only accept iso files    
            $mimes = array('iso' => 'text/iso');
            
            $upload_status = lw_handle_upload( $_FILES["file"], $upload_path, $mimes );
            
            if ( !$upload_status['error'] ) {
                
                echo '<form><input type="button" class="button" value="Continuar" onclick="continuar(\'' . $upload_status['file'] . '\');"></form>';
                
           }else{
                $status_message = $upload_status['error'];
               
                echo '<h1>' . $texts[$lang][$status_message] . '</h1>';
                
                echo '<form><input type="button" class="button" value="' . $texts[$lang]['back'] . '" onclick="history.back();"></form>';
           }    
       }         
    }else{
        
        $token = md5(uniqid(rand(), true));
        $_SESSION['token'] = $token;
        

    ?>
        <div class="upload-box">
            <form action="upload-iso.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="step" value="UPLOAD_ISO"/>
                <input type="hidden" name="token" value="<?php echo $token; ?>" />
                
                <label for="file"><?php echo $texts[$lang]['file']; ?></label>
                <input type="file" name="file" id="file" /> 
                <input type="submit" name="submit" value="<?php echo $texts[$lang]['send']; ?>" class="button" />
                <p>
                    <?php echo $texts[$lang]['max_allowed_file_size']; ?>: <?php echo $max_upload_size; ?>
                </p>
            </form>
        </div>

    <?php } ?>

   </body>
</html>
