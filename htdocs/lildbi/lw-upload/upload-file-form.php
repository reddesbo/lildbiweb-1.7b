<?php
/**
*  Receive the repetitive field fieldContent <fieldContent> that has the following format:
*    <electronic medium>^i<URL>
* 
*  and also its data input object.  Do the call to the Upload Module
*
* Parameters:
*
*  $content - field content
*  $fieldLabel - field Label
*  $lang - interface language
**/
session_start();

require ('config.php');
require ('functions.php');

/**
 *  getContent
 */ 
function getContent ($content, $sep) {
    if (strpos($content,$sep)>0) {
        $fieldContent = explode ($sep,$content);
    }
    return $fieldContent;
}

/**
 *  getFieldContent
 */ 
function getFieldContent ($content) {
    $sep = array(0=>"\r\n",1=>"\r",2=>"\n");

    $v='';
    $i=0;

    while ( (!$v) && ($i<count($sep)) ){
        $v = getContent ($content, $sep[$i]);
        $i++;
    }

    if ($v) {
        $r = $v;
    } else {
        $r[0] = $content;
    }

    return $r;
}

$lw_def = parse_ini_file('../lildbi.def.php');

$path_database = $lw_def['PATH_DATABASE'];
$lw_admin_url = 'http://' . $_SERVER['HTTP_HOST'] . '/cgi-bin/wxis1660.exe' . $lw_def['PATH_DATA'];

//check referer 
if ( !check_admin_referer($lw_admin_url) ){
    header("Status: 404 Not Found");
    exit();
}

$tag8 = $_GET['tag8'];
$fieldLabel = $_GET['fieldLabel'];
$lang = $_GET['lang'];
$result = $_REQUEST['result'];

$tabHeader = $_POST['tabHeader'];
$docId = $_POST['docId'];
$docFormerFileName = $_POST['docFormerFileName'];
$done = $_POST['done'];
$result = $_POST['result'];
$newItem = $_POST['newItem']; 
$newItemResult = $_POST['newItemResult'];
$closeLabel = $_POST['closeLabel'];


// generate session token
$token = md5(uniqid(rand(), true));
$_SESSION['token'] = $token;


if ( !$result ) {
    $input_item  = '<input type="hidden" name="cipar" value="'.$cipar.'"/>';
    $input_item .= '<input type="hidden" name="NO_REPLACE" value="true"/>';
    

    $i=0;
    $k = 0;
    $content = $tag8;

    $fieldContent = getFieldContent($content);

    foreach ($fieldContent as $item) {

        if ( substr($item,0,strlen($medium))==$medium ) {

            $url = substr($item,strlen($medium));
           
            if ( substr($url,0,strlen($url_data))==$url_data ){
                $docId[$i++] = substr($url,strpos($url,'id=')+3);
            }
        }
    }
    $i = 0;
    if ( count($docId) > 0 ) {
        foreach ($docId as $item) {
            $i++;
            $input_item.= '
                <input type="hidden" name="label" value="'.$fieldLabel.'"/>
                <input type="hidden" name="docId" value="'.$item.'"/>
                <input type="hidden" name="docRoot" value="'.$document_root.'"/>
                <input type="hidden" name="docRootSubdir" value="'.$subdir.'"/>
                <input type="hidden" name="databasePath" value="'. $path_database .'"/>
                <input type="hidden" name="method" value="put"/>';
        }
    }    
    $input_item.= '
            <input type="hidden" name="label" value="'.$fieldLabel.'"/>
            <input type="hidden" name="docId" value="new"/>
            <input type="hidden" name="docRoot" value="'.$document_root.'"/>
            <input type="hidden" name="docRootSubdir" value="'.$subdir.'"/>
            <input type="hidden" name="databasePath" value="'. $path_database .'"/>
            <input type="hidden" name="method" value="put"/>';

    $b = 1;
    if ($b) {
        $onload  = 'document.formUpload.submit();';
    } else {
        $submit_button = '<input type="submit" name="button" value="submit"/>' ;
    }

    $form_begin = '
    <form name="formUpload" action="' . $wxis .'" method="post">
        <input type="hidden" name="IsisScript" value="'.$IsisScript.'"/>
        <input type="hidden" name="token" value="'.$token.'"/>
        <input type="hidden" name="lang" value="'.$lang.'"/>';
        
    $form_end = '</form>';

    $page_content = $form_begin . $input_item . $submit_button . $form_end;

} else {
    
   
    if ($result == 'close') {
        $onload  = 'window.close();';
        
    } else {
        $mif = "''";

        if (strlen($_POST["myFullText"][0])>0)
            $mif = "'^g".$_POST["myFullText"][0]."'";

            $uploadBaseURL="'".$medium.$url_data."'" ;

            $medium = "'".$medium."'";
            $url_data = "'".$url_data."'";

            $onload  = 'javascript:add( window.opener.document.forms[0].tag08,'.$uploadBaseURL.','.$mif.',document.forms[0]);';

            $page_content.='<form>
                    <table align="center" border="0">
                <tr>
                    <td>
                        <table align="center" border="1">
                            <tr class="tabHeader">';

            foreach ($tabHeader as $item){
                $page_content.= '<td>'.$item.'</td>';
            }
            $page_content.='</tr>';
            
            reset($docFormerFileName);
            reset($done);
            reset($result);
            reset($newItem);
            reset($newItemResult);
            
            foreach ($docId as $item) {

                $page_content.= '<tr>                    
                                    <td class="docId">'.$item.'</td>
                                    <td class="docFormerFileName">'.current($docFormerFileName).'</td>
                                    <td class="return">'.current($result).'</td>
                                </tr>
                                </table>
                            </td>
                            </tr>
                                <tr><td>&#160;</td></tr>
                                    <input type="hidden" name="done" value="'.current($done).'"/>
                                    <input type="hidden" name="newItem" value="'.current($newItem).'"/>
                                    <input type="hidden" name="newItemResult" value="'.current($newItemResult).'"/>';
                $page_content.= '<tr><td  align="right"><input type="submit" value="'.$closeLabel.'" onClick="window.close();"/></td></tr>';
                next($result);
                next($done);
                next($docFormerFileName);
                next($newItem);
                next($newItemResult);
            }
            $page_content.='</table></form>';
    }
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>            
        <link rel="stylesheet" href="<?php echo $lw_def['PATH_DATA'] ?>/lw-upload/upload.css">
        <script type="text/javascript" src="<?php echo $lw_def['PATH_DATA'] ?>/lw-upload/upload.js"> </script>
    </head>
    
    <body onLoad="<?php echo $onload; ?>" >
    
        <?php echo $page_content; ?>
    
    </body>
</html>    
    
