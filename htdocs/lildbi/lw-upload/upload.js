

function getNewItemValue  (done,result,item,uploadBaseURL,val) {

    if (result == '0') {
        switch (done) {
            case 'Save':
                v =  uploadBaseURL + item;
                break;
            case 'Delete':
                v = '';
                break;
            case 'Cancel': 
                v = val;
                break;
            case '':
                v = '';
                break;
        }
    } else {
        v = val;
    }
    if (v.length>0) v =v+'\r';
    return v;
}

function initArray (obj){
    array = new Array();
    if (obj.length) {
        array = obj;
    } else {
        array[0] = obj;
    }
    return array;
}

function add (inputField,uploadBaseURL,fulltext,resultForm) {
    v = '';
    r = '';
    if (resultForm.done.value == 'Delete') {
        oldContent = inputField.value.split ('\r\n');
        replaced = false;
        i = 0;
        while ( (!replaced) && (i<oldContent.length)) {

            if (oldContent[i]==uploadBaseURL+resultForm.newItem.value) {
                replaced = true;
            } else {
                if (oldContent[i].length) {
                    v = v  + r + oldContent[i] ;
                    r = '\r\n';
                }
            }
            i++;
        }
        for (k = i; k<oldContent.length; k++) {
            if (oldContent[k].length) {
                v = v  + r + oldContent[k] ;
                r = '\r\n';
            }
        }
    } else {
        oldContent = inputField.value.split ('\r\n');
        for (k = 0; k<oldContent.length; k++) {
            if (oldContent[k].length) {
                v = v  + r + oldContent[k] ;
                r = '\r\n';
            }
        }
        v = v + r + uploadBaseURL+ resultForm.newItem.value+fulltext;
    }
    inputField.value = v;
    return true;

}
